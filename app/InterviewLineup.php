<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewLineup extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function cv()
    {
        return $this->belongsTo('App\CvBank','cv_id');
    }

    public function panel()
    {
        return $this->hasMany('App\InterviewPanel', 'interview_lineup_id');
    }

}
