<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function unitType()
    {
        return $this->belongsTo('App\DbVariableDetail', 'type');
    }
}
