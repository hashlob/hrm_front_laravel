<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAllowance extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function allowanceType()
    {
        return $this->belongsTo('App\DbVariableDetail', 'allowance_type_id');
    }

}
