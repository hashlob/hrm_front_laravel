<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveConfiguration extends Model
{
    protected $table = 'leave_configuration';
    protected $guarded = ['id', 'created_at', 'updated_at'];


    public function leaveConfigurations()
    {
        return $this->hasMany('App\LeaveConfigurationDetails');
    }
}
