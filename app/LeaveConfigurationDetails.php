<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveConfigurationDetails extends Model
{
    protected $table = 'leave_configuration_details';
    protected $guarded = ['id', 'created_at', 'updated_at'];


    public function leaveType()
    {
        return $this->belongsTo('App\DbVariableDetail','leave_type');
    }

    public function configType()
    {
        return $this->belongsTo('App\DbVariableDetail','config_id');
    }
}
