<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbVariable extends Model
{
    public function details()
    {
        return $this->hasMany('App\DbVariableDetail','type_id');
    }
}
