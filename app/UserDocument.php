<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function document()
    {
        return $this->belongsTo('App\DbVariableDetail', 'document_type_id');
    }
}
