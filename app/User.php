<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;


class User extends Model
{
    // use Notifiable;
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function systemDetail()
    {
        return $this->hasOne('App\UserSystemDetail');
    }

    public function employmentDetails()
    {
        return $this->hasOne('App\EmploymentDetail');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }
  
}
