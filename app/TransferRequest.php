<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferRequest extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function initiator()
    {
        return $this->belongsTo('App\User', 'initiated_by');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function rejector()
    {
        return $this->belongsTo('App\User', 'rejected_by');
    }

    public function nextInline()
    {
        return $this->belongsTo('App\User', 'next_inline');
    }

    public function from()
    {
        return $this->belongsTo('App\Unit', 'from');
    }
    public function to()
    {
        return $this->belongsTo('App\Unit', 'to');
    }

}
