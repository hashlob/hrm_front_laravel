<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbVariableDetail extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table =  "db_variables_details";
}
