<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewPanel extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function interviewers()
    {
        return $this->belongsTo('App\User', 'interviewer_id');
    }
}
