<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function manager()
    {
        return $this->belongsTo('App\User');
    }

    public function requestLine()
    {
        return $this->hasMany('App\RequestLine');
    }

}
