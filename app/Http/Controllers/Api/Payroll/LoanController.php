<?php

namespace App\Http\Controllers\Api\Payroll;

use App\Http\Controllers\Controller;
use App\Loan;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function addLoan(Request $request)
    {
        $user_id = $request->user_id;
        $type_id = $request->type_id;
        $amount = $request->amount;
        $installments = $request->installments;
        $start_month = $request->start_month;
        $year = $request->year;
        $description = $request->description;
        $id = $request->id;
        $loan = null;
        $message = null;
        $status = "0";

        if ($start_month == date("m")) {
            $status = "2";
        }

        if ($id == null) {
            $loan = Loan::create([
                'user_id' => $user_id,
                'type_id' => $type_id,
                'amount' => $amount,
                'installments' => $installments,
                'current_installment' => 0,
                'start_month' => $start_month,
                'year' => $year,
                'description' => $description,
                'status' => $status,
            ]);

            $message = "Loan Assigned.";
        } else {
            $loan = Loan::where('id', $id)
                ->update([
                    'amount' => $amount,
                    'installments' => $installments,
                    'year' => $year,
                    'start_month' => $start_month,
                    'description' => $description,
                ]);

            $message = "Loan details updated.";
        }

        return response()->json([
            "data" => $loan,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getLoans(Request $request)
    {
        $loans = Loan::with('user', 'type');

        $loans = $loans->get();
        return response()->json([
            "data" => $loans,
            "code" => $this->successStatus,
            "message" => "loans",
        ], $this->successStatus);
    }

    public function getLoanById(Request $request)
    {
        $id = $request->id;
        $loan = Loan::with('user', 'type')
            ->where('id', $id)
            ->get()->first();
        return response()->json([
            "data" => $loan,
            "code" => $this->successStatus,
            "message" => "loan",
        ], $this->successStatus);
    }
}
