<?php

namespace App\Http\Controllers\Api\Payroll;

use App\Bonus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BonusController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function addBonus(Request $request)
    {

        $user_id = $request->user_id;
        $type_id = $request->type_id;
        $month = $request->month;
        $year = $request->year;
        $amount = $request->amount;
        $description = $request->description;
        $id = $request->id;
        $bonus = null;
        $message = "";

        if ($id == null) {
            $bonus = Bonus::create([
                'user_id' => $user_id,
                'type_id' => $type_id,
                'month' => $month,
                'year' => $year,
                'description' => $description,
                'amount' => $amount,
            ]);

            $message = "Bonus Added.";
        } else {
            $bonus = Bonus::where('id', $id)
                ->update([
                    'month' => $month,
                    'year' => $year,
                    'description' => $description,
                    'amount' => $amount,
                ]);

            $message = "Bonus Updated.";

        }

        return response()->json([
            "data" => $bonus,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);

    }

    public function getBonuses(Request $request)
    {
        $bonuses = Bonus::with('user', 'type');

        $bonuses = $bonuses->get();
        return response()->json([
            "data" => $bonuses,
            "code" => $this->successStatus,
            "message" => "Bonuses",
        ], $this->successStatus);
    }

    public function getBonusById(Request $request)
    {
        $id = $request->id;

        $bonus = Bonus::with('user', 'type')
            ->where('id', $id)
            ->get()->first();
        return response()->json([
            "data" => $bonus,
            "code" => $this->successStatus,
            "message" => "bonus",
        ], $this->successStatus);
    }

    public function removeBonus(Request $request)
    {
        $id = $request->id;
        $bonus = Bonus::where('id', $id)
            ->delete();
        return response()->json([
            "data" => $bonus,
            "code" => $this->successStatus,
            "message" => "Bonus.",
        ], $this->successStatus);
    }
}
