<?php

namespace App\Http\Controllers\Api\Payroll;

use App\Bonus;
use App\Deduction;
use App\EmploymentDetail;
use App\Fine;
use App\Http\Controllers\Controller;
use App\Loan;
use App\Payroll;
use App\SalaryIncreamentHistory;
use App\User;
use App\UserAllowance;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function markSalaryIncreament(Request $request)
    {
        $user_id = $request->user_id;
        $amount = $request->amount;
        $date = $request->date;
        $description = $request->description;

        $empl_details = EmploymentDetail::where('user_id', $user_id)
            ->get()
            ->first();
        if ($empl_details == null) {
            return response()->json([
                'errors' => [],
                'code' => $this->validationStatus,
                'message' => "No employment details added.",
            ], $this->validationStatus);
        }

        $employment_detail_id = $empl_details->id;
        $current_salary = $empl_details->salary;
        $check_salary_increament = SalaryIncreamentHistory::where([
            'user_id' => $user_id,
            'employment_detail_id' => $employment_detail_id,
        ])->get();

        if (sizeof($check_salary_increament) == 0) {
            SalaryIncreamentHistory::create([
                'user_id' => $user_id,
                'amount' => $current_salary,
                'date' => $date,
                'description' => $description,
                'employment_detail_id' => $employment_detail_id,
            ]);
        }
        $current_salary = $current_salary + $amount;
        EmploymentDetail::where('user_id', $user_id)
            ->update([
                'salary' => $current_salary,
            ]);
        $markIncreament = SalaryIncreamentHistory::create([
            'user_id' => $user_id,
            'amount' => $amount,
            'date' => $date,
            'description' => $description,
            'employment_detail_id' => $employment_detail_id,
        ]);

        return response()->json([
            "data" => $markIncreament,
            "code" => $this->successStatus,
            "message" => "Increament marked.",
        ], $this->successStatus);

    }

    public function generatePayroll(Request $request)
    {
        $month = $request->month;
        $year = $request->year;

        $payroll_report = Payroll::with('user')
            ->where([
                'month' => $month,
                'year' => $year,
            ])->get();

        if (sizeof($payroll_report) > 0) {
            return response()->json([
                "data" => $payroll_report,
                "code" => $this->successStatus,
                "message" => "Payroll report.",
            ], $this->successStatus);
        }

        $payroll_report = [];
        $employees = User::where('id', '<>', 1)
            ->with('employmentDetails')
            ->get();

        foreach ($employees as $key => $employee) {
            $employee_id = $employee->id;
            $employee_number = $employee->employee_number;
            $employee_name = $employee->first_name . " " . $employee->first_name;
            $employee_salary = $employee->employmentDetails == null ? null : $employee->employmentDetails->salary;

            $employee_bonuses = Bonus::where([
                'year' => $year,
                'month' => $month,
                'user_id' => $employee_id,
            ])->get()->sum('amount');

            $employee_fines = Fine::where([
                'year' => $year,
                'month' => $month,
                'user_id' => $employee_id,
            ])->get()->sum('amount');

            $employee_deductions = Deduction::where([
                'year' => $year,
                'month' => $month,
                'user_id' => $employee_id,
            ])->get()->sum('amount');

            $user_allowances = UserAllowance::where([
                'user_id' => $employee_id,
                'status' => "1",
            ])->get();

            $user_loans = Loan::where([
                'user_id' => $employee_id,
                'status' => "2",
            ])->get();

            $employee_loans = 0;
            foreach ($user_loans as $key => $user_loan) {
                $loan_due_date = strtotime('+' . $user_loan->installments . " months", strtotime("01-" . $user_loan->start_month . "-" . $user_loan->year));
                $loan_date = strtotime("01-" . $user_loan->start_month . "-" . $user_loan->year);
                $current_date = strtotime("01-$month-$year");
                if ($current_date >= $loan_date && $current_date <= $loan_due_date) {
                    $employee_loans += $user_loan->amount;
                }
            }

            $employee_allowances = 0;
            foreach ($user_allowances as $key => $user_allowance) {
                $allowance_value = $user_allowance->value;
                if ($user_allowance->value_type == "Percent") {
                    $employee_allowances += ($allowance_value / 100) * $employee_salary;
                } elseif ($user_allowance->value_type == "Amount") {
                    $employee_allowances += $allowance_value;
                }
            }
            $all_additions = $employee_bonuses + $employee_allowances + $employee_salary;
            $all_reductions = $employee_fines + $employee_deductions + $employee_loans;
            $net_salary = $all_additions - $all_reductions;

            $employee_array = array(
                'employee_id' => $employee_id,
                'employee_number' => $employee_number,
                'employee_name' => $employee_name,
                'employee_salary' => $employee_salary,
                'employee_bonuses' => $employee_bonuses,
                'employee_fines' => $employee_fines,
                'employee_deductions' => $employee_deductions,
                'employee_allowances' => $employee_allowances,
                'employee_all_allowances' => $user_allowances,
                'employee_loans' => $employee_loans,
                'all_additions' => $all_additions,
                'all_reductions' => $all_reductions,
                'net_salary' => $net_salary,
                'year' => $year,
                'month' => $month,
            );
            array_push($payroll_report, $employee_array);
        }
        return response()->json([
            "data" => $payroll_report,
            "code" => $this->successStatus,
            "message" => "Increament marked.",
        ], $this->successStatus);
    }

    public function savePayroll(Request $request)
    {
        $payroll_details = $request->payroll_details;
        $month = $request->month;
        $year = $request->year;

        $check_payroll = Payroll::where([
            'month' => $month,
            'year' => $year,
        ])->get();

        if (sizeof($check_payroll) > 0) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Payroll Already saved.",
            ], $this->validationStatus);
        }

        foreach ($payroll_details as $key => $payroll_detail) {
            $employee_id = $payroll_detail['employee_id'];
            $employee_number = $payroll_detail['employee_number'];
            $employee_name = $payroll_detail['employee_name'];
            $employee_salary = $payroll_detail['employee_salary'];
            $employee_bonuses = $payroll_detail['employee_bonuses'];
            $employee_fines = $payroll_detail['employee_fines'];
            $employee_deductions = $payroll_detail['employee_deductions'];
            $employee_allowances = $payroll_detail['employee_allowances'];
            $employee_loans = $payroll_detail['employee_loans'];
            $all_additions = $payroll_detail['all_additions'];
            $all_reductions = $payroll_detail['all_reductions'];
            $net_salary = $payroll_detail['net_salary'];

            $user_allowance = UserAllowance::where([
                'user_id' => $employee_id,
                'status' => "1",
                'is_monthly' => 0,
            ])->update([
                'status' => "0",
            ]);

            $user_loans = Loan::where([
                'user_id' => $employee_id,
                'status' => '2',
            ])->get();

            foreach ($user_loans as $key => $user_loan) {
                $loan_due_date = strtotime('+' . $user_loan->installments . " months", strtotime("01-" . $user_loan->start_month . "-" . $user_loan->year));
                $loan_date = strtotime("01-" . $user_loan->start_month . "-" . $user_loan->year);
                $current_date = strtotime("01-$month-$year");
                $installments = $user_loan->installments;
                $current_installment = $user_loan->current_installment;
                $status = $user_loan->status;

                if ($current_date >= $loan_date && $current_date <= $loan_due_date) {
                    $current_installment += 1;
                    if ($current_installment == $installments) {
                        $status = '1';
                    }

                    Loan::where('id', $user_loan->id)
                        ->update([
                            'current_installment' => $current_installment,
                            'status' => $status,
                        ]);

                }
            }
            Payroll::create([
                'employee_id' => $employee_id,
                'employee_number' => $employee_number,
                'employee_name' => $employee_name,
                'employee_salary' => $employee_salary,
                'employee_bonuses' => $employee_bonuses,
                'employee_fines' => $employee_fines,
                'employee_deductions' => $employee_deductions,
                'employee_allowances' => $employee_allowances,
                'employee_loans' => $employee_loans,
                'all_additions' => $all_additions,
                'all_reductions' => $all_reductions,
                'net_salary' => $net_salary,
                'month' => $month,
                'year' => $year,
            ]);

        }

        return response()->json([
            "data" => [],
            "code" => $this->successStatus,
            "message" => "Payroll Saved.",
        ], $this->successStatus);
    }

    public function lockPayroll(Request $request)
    {

        $month = $request->month;
        $year = $request->year;

        $check_payroll = Payroll::where([
            'month' => $month,
            'year' => $year,
        ])->get();

        if (sizeof($check_payroll) == 0) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Save Payroll First.",
            ], $this->validationStatus);
        }

        $update_status = Payroll::where([
            'month' => $month,
            'year' => $year,
        ])->update([
            'status' => '1',
        ]);

        return response()->json([
            "data" => $update_status,
            "code" => $this->successStatus,
            "message" => "Payroll Locked.",
        ], $this->successStatus);
    }

    public function getEmployeePayslip(Request $request)
    {
        $user_id = $request->user_id;
        $month = $request->month;
        $year = $request->year;

        $payroll = Payroll::with('user', 'user.employmentDetails', 'user.employmentDetails.department', 'user.employmentDetails.unit', 'user.employmentDetails.designation')
            ->where([
                'month' => $month,
                'year' => $year,
                'employee_id' => $user_id,
                'status' => "1",
            ])
            ->get()->first();

        if ($payroll == null) {
            return response()->json([
                "data" => $payroll,
                "code" => $this->successStatus,
                "message" => "Payroll not generated yet.",
            ], $this->successStatus);
        }

        $bonuses = Bonus::with('type')
            ->where([
                'year' => $year,
                'month' => $month,
                'user_id' => $user_id,
            ])->get();

        $fines = Fine::with('type')
            ->where([
                'year' => $year,
                'month' => $month,
                'user_id' => $user_id,
            ])->get();

        $deductions = Deduction::with('type')
            ->where([
                'year' => $year,
                'month' => $month,
                'user_id' => $user_id,
            ])->get();

        $payslip = array(
            'payroll' => $payroll,
            'bonuses' => $bonuses,
            'fines' => $fines,
            'deductions' => $deductions,
        );

        return response()->json([
            "data" => $payslip,
            "code" => $this->successStatus,
            "message" => "Payslip.",
        ], $this->successStatus);
    }
}
