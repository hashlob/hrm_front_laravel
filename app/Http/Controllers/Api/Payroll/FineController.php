<?php

namespace App\Http\Controllers\Api\Payroll;

use App\Fine;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FineController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function addFine(Request $request)
    {
        $user_id = $request->user_id;
        $type_id = $request->type_id;
        $month = $request->month;
        $year = $request->year;
        $amount = $request->amount;
        $description = $request->description;
        $id = $request->id;
        $fine = null;
        $message = "";

        if ($id == null) {
            $fine = Fine::create([
                'user_id' => $user_id,
                'type_id' => $type_id,
                'month' => $month,
                'year' => $year,
                'description' => $description,
                'amount' => $amount,
            ]);

            $message = "Fine Added.";
        } else {
            $fine = Fine::where('id', $id)
                ->update([
                    'month' => $month,
                    'year' => $year,
                    'description' => $description,
                    'amount' => $amount,
                ]);

            $message = "Fine Updated.";

        }

        return response()->json([
            "data" => $fine,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getFines(Request $request)
    {
        $fines = Fine::with('user', 'type');

        $fines = $fines->get();
        return response()->json([
            "data" => $fines,
            "code" => $this->successStatus,
            "message" => "Fines",
        ], $this->successStatus);
    }

    public function getFineById(Request $request)
    {

        $id = $request->id;
        $fine = Fine::with('user', 'type')
            ->where('id', $id)
            ->get()->first();
        return response()->json([
            "data" => $fine,
            "code" => $this->successStatus,
            "message" => "fine",
        ], $this->successStatus);
    }

    public function removeFine(Request $request)
    {
        $id = $request->id;
        $fine = Fine::where('id', $id)
            ->delete();
        return response()->json([
            "data" => $fine,
            "code" => $this->successStatus,
            "message" => "fine.",
        ], $this->successStatus);
    }
}
