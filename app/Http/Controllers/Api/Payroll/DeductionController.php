<?php

namespace App\Http\Controllers\Api\Payroll;

use App\Deduction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeductionController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function addDeduction(Request $request)
    {

        $user_id = $request->user_id;
        $type_id = $request->type_id;
        $month = $request->month;
        $year = $request->year;
        $amount = $request->amount;
        $description = $request->description;
        $id = $request->id;
        $deduction = null;
        $message = "";

        if ($id == null) {
            $deduction = Deduction::create([
                'user_id' => $user_id,
                'type_id' => $type_id,
                'month' => $month,
                'year' => $year,
                'description' => $description,
                'amount' => $amount,
            ]);

            $message = "Deduction Added.";
        } else {
            $deduction = Deduction::where('id', $id)
                ->update([
                    'month' => $month,
                    'year' => $year,
                    'description' => $description,
                    'amount' => $amount,
                ]);

            $message = "Deduction Updated.";

        }

        return response()->json([
            "data" => $deduction,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getDeductions(Request $request)
    {
        $deductions = Deduction::with('user', 'type');

        $deductions = $deductions->get();
        return response()->json([
            "data" => $deductions,
            "code" => $this->successStatus,
            "message" => "Deductions",
        ], $this->successStatus);
    }

    public function getDeductionById(Request $request)
    {
        $id = $request->id;
        $deduction = Deduction::with('user', 'type')
            ->where('id', $id)
            ->get()->first();
        return response()->json([
            "data" => $deduction,
            "code" => $this->successStatus,
            "message" => "deduction",
        ], $this->successStatus);
    }

    public function removeDeduction(Request $request)
    {
        $id = $request->id;
        $deduction = Deduction::where('id', $id)
            ->delete();
        return response()->json([
            "data" => $deduction,
            "code" => $this->successStatus,
            "message" => "Deduction.",
        ], $this->successStatus);
    }
}
