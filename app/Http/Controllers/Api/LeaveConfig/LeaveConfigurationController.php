<?php

namespace App\Http\Controllers\Api\LeaveConfig;

use App\Http\Controllers\Controller;
use App\LeaveConfiguration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LeaveConfigurationController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {
        $rules = [
            "config_name" => "required",
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'data' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }

        $id = $request->id;
        $leaveConfig = null;
        if ($id == null) {
            $leaveConfig = LeaveConfiguration::create($request->all());
            $message = "Leave Configuration created.";
        } else {

            $leaveConfig = LeaveConfiguration::where('id', $id)
                ->update([
                    'config_name' => $request->config_name
                ]);
            $message = "Leave Configuration updated.";
        }

        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $leaveConfig = LeaveConfiguration::find($id);
        $leaveConfig->delete();
        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration Deleted",
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
        $leaveConfig = LeaveConfiguration::all();

        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration",
        ], $this->successStatus);

    }
}
