<?php

namespace App\Http\Controllers\Api\LeaveConfig;

use App\Http\Controllers\Controller;
use App\LeaveConfigurationDetails;
use Illuminate\Http\Request;

class LeaveConfigurationDetailsController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        
        $id = $request->id;
        $config_id = $request->config_id;
        $leave_type = $request->leave_type;
        $leaveConfig = null;

        $leaveConfigCount = LeaveConfigurationDetails::where(['leave_type' => $leave_type, 'config_id' => $config_id])->get();
        if (sizeof($leaveConfigCount) == 0) {
            $leaveConfig = LeaveConfigurationDetails::create($request->all());
            $message = "Leave Configuration created.";
        } else {
            $leaveConfig = LeaveConfigurationDetails::where(['leave_type' => $leave_type, 'config_id' => $config_id])
                ->update([
                    'quantity' => $request->quantity,
                ]);
            $message = "Leave Configuration updated.";
        }

        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $leaveConfig = LeaveConfigurationDetails::find($id);
        $leaveConfig->delete();
        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration Deleted",
        ], $this->successStatus);
    }

    public function getForConfig(Request $request)
    {
        $config_id = $request->config_id;
        $leaveConfig = LeaveConfigurationDetails::with('leaveType')
            ->where('config_id', $config_id)->get();

        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration",
        ], $this->successStatus);

    }
}
