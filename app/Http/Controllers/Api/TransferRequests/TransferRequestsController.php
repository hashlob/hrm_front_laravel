<?php

namespace App\Http\Controllers\Api\TransferRequests;

use App\EmploymentDetail;
use App\Http\Controllers\Controller;
use App\Notification;
use App\RequestLine;
use App\TransferRequest;
use App\Zone;
use Illuminate\Http\Request;

class TransferRequestsController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";
    public $request_line_type = 11;

    public function generateRequest(Request $request)
    {

        $to = $request->to;
        $request_date = $request->request_date;
        $reason = $request->reason;
        $user_id = $request->user_id;
        $initiated_by = $request->initiated_by;
        $department_id = null;
        $transfer_request = [];
        $from = null;
        $message = null;

        $empDetails = EmploymentDetail::where('user_id', $user_id)->get()->first();
        $department_id = $empDetails["department_id"];

        $zoneManagerCheck = Zone::where('manager_id', $initiated_by)
            ->get();

        if ($zoneManagerCheck->count() == 0) {
            return response()->json([
                "data" => $transfer_request,
                "code" => $this->validationStatus,
                "message" => "Only zonal manager allowed for this action.",
            ], $this->validationStatus);
        }

        $requestLine = RequestLine::where([
            'department_id' => $department_id,
            'request_line_type' => $this->request_line_type,
        ])
            ->orderBy('order', 'ASC')
            ->get()->first();

        if ($requestLine == null) {
            return response()->json([
                "data" => $transfer_request,
                "code" => $this->validationStatus,
                "message" => "Request Line not defined.",
            ], $this->validationStatus);
        }

        $next_inline = $requestLine->manager_id;

        $user_unit = EmploymentDetail::where('user_id', $user_id)
            ->get()
            ->first();
        $from = $user_unit->unit_id;

        $transferCheck = TransferRequest::where('user_id', $user_id)
            ->where('status', '<>', 1)
            ->get();
        if (sizeof($transferCheck) == 0) {
            $transfer_request = TransferRequest::create([
                'to' => $to,
                'from' => $from,
                'request_date' => $request_date,
                'reason' => $reason,
                'user_id' => $user_id,
                'status' => 2,
                'initiated_by' => $initiated_by,
                'next_inline' => $next_inline,
            ]);
            $message = "Request generated.";
        } else {
            return response()->json([
                "data" => $transfer_request,
                "code" => $this->validationStatus,
                "message" => "Request already generated.",
            ], $this->validationStatus);
        }

        return response()->json([
            "data" => $transfer_request,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getTransferRequests(Request $request)
    {

        $status = $request->status;
        $user_id = $request->user_id;

        $transfer_request = TransferRequest::select('*')
            ->with('initiator', 'user', 'from', 'to', 'rejector', 'nextInline');

        if ($status != null) {
            $transfer_request = $transfer_request->where('status', $status);
        }
        if ($user_id != null) {
            $transfer_request = $transfer_request->where('user_id', $user_id);
        }

        $transfer_request = $transfer_request->get();

        return response()->json([
            "data" => $transfer_request,
            "code" => $this->successStatus,
            "message" => "Transfer Requests.",
        ], $this->successStatus);
    }

    public function processTransferRequest(Request $request)
    {

        $transfer_id = $request->transfer_id;
        $status = $request->status;
        $reason = $request->reason;
        $manager_id = $request->manager_id;
        $department_id = $request->department_id;

        $transfer_request = null;
        $message = null;

        $requestLine = RequestLine::where([
            'manager_id' => $manager_id,
            'request_line_type' => $this->request_line_type,
        ])->orderBy('order', 'ASC')
            ->get();
        if (sizeof($requestLine) == 0) {
            return response()->json([
                "data" => $requestLine,
                "code" => $this->unauthorisedStatus,
                "message" => "Action not allowed.",
            ], $this->unauthorisedStatus);
        }

        $user_unit = TransferRequest::where('id', $transfer_id)
            ->get()->first();
        $user_id = $user_unit->unit_id;
        $to = $user_unit->to;

        if ($status == 0) {
            $transfer_request = TransferRequest::where('id', $transfer_id)
                ->update([
                    'rejected_by' => $manager_id,
                    'reason' => $reason,
                    'status' => $status,
                ]);

            $message = "Transfer Rejected.";
        } else {
            $transfer_request = TransferRequest::where('id', $transfer_id)
                ->update([
                    'status' => $status,
                ]);

            $user_dept = EmploymentDetail::where('user_id', $user_id)
                ->update([
                    "unit_id" => $to,
                ]);

            $message = "Transfer Complete.";
        }

        return response()->json([
            "data" => $transfer_request,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function checkTransfers(Request $request)
    {
        date_default_timezone_set("Asia/Karachi");

        $transfer_users = EmploymentDetail::select('user_id', 'is_tranfer_allow', 'transfer_period', 'next_transfer', 'department_id')
            ->with('user')
            ->where('is_tranfer_allow', 1)
            ->get();

        foreach ($transfer_users as $key => $employee) {
            $next_transfer = $employee->next_transfer;
            $transfer_period = $employee->transfer_period;
            $user_id = $employee->user_id;
            $name = $employee->user->first_name . " " . $employee->user->last_name;
            $department_id = $employee->department_id;

            $today = date("Y-m-d");
            $current_date = strtotime($today);
            $next_transfer = strtotime($next_transfer);
            // $next_transfer = strtotime("2020-04-06");
            $diff = $next_transfer - $current_date;

            if ($diff == 0) {
                $new_next_transfer = strtotime($employee->next_transfer . $transfer_period . "days");
                $new_next_transfer = date("Y-m-d", $new_next_transfer);
                $title = "Transfer Request";
                $message = "Transfer period for $name has been completed.";

                EmploymentDetail::where('user_id', $user_id)
                    ->update([
                        'next_transfer' => $new_next_transfer,
                    ]);

                $request_lines = RequestLine::where([
                    'request_line_type' => $this->request_line_type,
                    'department_id' => $department_id,
                ])
                    ->get();

                if (sizeof($request_lines) > 0) {

                    foreach ($request_lines as $key => $request_line) {

                        $noti = Notification::create([
                            'to' => $request_line->manager_id,
                            'date' => $today,
                            'message' => $message,
                            'title' => $title,
                            'path' => "/tranfermanagement/requesttransfer",
                        ]);
                    }
                }

            }
        }

        return response()->json([
            "data" => [],
            "code" => $this->successStatus,
            "message" => "users for transfer",
        ], $this->successStatus);
    }

}
