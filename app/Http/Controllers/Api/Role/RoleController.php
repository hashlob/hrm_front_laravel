<?php

namespace App\Http\Controllers\Api\Role;

use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        $rules = [
            "name" => "required|unique:roles",
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'data' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }

        $id = $request->id;
        $message = "";
        if ($id == null) {
            $role = Role::create($request->all());
            $message = "Role created.";
        } else {
            $role = Role::where('id',$id)
                ->update([
                    'name' => $request->name
                ]);
            $message = "Role updated.";

        }

        return response()->json([
            "data" => $role,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
        $roles = Role::all();

        return response()->json([
            "data" => $roles,
            "code" => $this->successStatus,
            "message" => "Roles.",
        ], $this->successStatus);

    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $role = Role::destroy($id);

        return response()->json([
            "data" => $role,
            "code" => $this->successStatus,
            "message" => "Role Deleted.",
        ], $this->successStatus);
    }

    public function getRoleById(Request $request)
    {
        $id = $request->id;
        $role = Role::find($id);

        return response()->json([
            "data" => $role,
            "code" => $this->successStatus,
            "message" => "Role.",
        ], $this->successStatus);
    }

}
