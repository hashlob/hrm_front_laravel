<?php

namespace App\Http\Controllers\Api\Machine;

use App\Http\Controllers\Controller;
use App\Machine;
use Illuminate\Http\Request;

class MachineController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        $ip = $request->ip;
        $branch_name = $request->branch_name;
        $id = $request->id;
        $message = '';
        $machine = null;

        if ($id == null) {
            $machine = Machine::create([
                'ip' => $ip,
                'branch_name' => $branch_name,
            ]);
            $message = "Machine Created.";
        } else {
            $machine = Machine::where('id', $id
            )
                ->update([
                    'ip' => $ip,
                    'branch_name' => $branch_name,
                ]);
            $message = "Machine Updated.";

        }

        return response()->json([
            "data" => $machine,
            "code" => $this->successStatus,
            "message" => $message,
        ]);
    }

    public function getMachines(Request $request)
    {

        $machines = Machine::all();

        return response()->json([
            "data" => $machines,
            "code" => $this->successStatus,
            "message" => "Machines",
        ]);
    }
}
