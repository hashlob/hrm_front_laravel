<?php

namespace App\Http\Controllers\Api\Shared;

use App\DbVariable;
use App\DbVariableDetail;
use App\Department;
use App\Designation;
use App\EmploymentDetail;
use App\Http\Controllers\Controller;
use App\LeaveRequest;
use App\Notification;
use App\Role;
use App\Unit;
use App\User;
use App\UserSystemDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SharedController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function getUsersForSelect(Request $request)
    {
        $users = null;

        if ($request->userType) {

            $users = UserSystemDetail::where('role_id', $request->userType)
                ->with('user')
                ->get();
        }

        if ($request->department) {

            $departUser = Department::select('manager_id')
                ->whereNotNull('manager_id')
                ->get();

            $userIds = [];

            foreach ($departUser as $key => $value) {
                $userIds[$key] = $value->manager_id;
            }

            $users = USER::whereNotIn('id', $userIds)
                ->with('systemDetail')
                ->get();
        }

        if ($request->requestLine) {

            $departUser = UserSystemDetail::whereIn('role_id', [2, 3])
                ->get();

            $userIds = [];

            foreach ($departUser as $key => $value) {
                $userIds[$key] = $value->user_id;
            }

            $users = User::whereIn('id', $userIds)
                ->get();
        }

        if ($request->leaveSelect) {

            $date = $request->date;

            $leave_request = LeaveRequest::select('applied_by')
                ->where('from', '<=', $date)
                ->where('to', '>=', $date)
                ->get();

            $userIds = [];

            foreach ($leave_request as $key => $value) {
                array_push($userIds, $value->applied_by);
            }

            $users = USER::whereNotIn('id', $userIds)
                ->get();
        }
        if ($request->all) {
            $users = User::where('id', '<>', 1)
                ->get();
        }

        if ($request->transferRequest) {
            $users = EmploymentDetail::select('user_id')
                ->with('user')
                ->where('is_tranfer_allow', 1)
                ->get();
        }

        return response()->json([
            "data" => $users,
            "code" => $this->successStatus,
            "message" => "Users.",
        ]);
    }

    public function getRolesForSelect(Request $request)
    {

        $roles = null;

        if ($request->all) {
            $roles = Role::all();
        } else {
            $roles = Role::where('id', '<>', 1)
                ->get();
        }

        return response()->json([
            "data" => $roles,
            "code" => $this->successStatus,
            "message" => "Roles.",
        ]);
    }

    public function getUnitForSelect(Request $request)
    {
        $roles = Unit::all();

        return response()->json([
            "data" => $roles,
            "code" => $this->successStatus,
            "message" => "Roles.",
        ]);
    }

    public function getDepartmentForSelect(Request $request)
    {
        $roles = Department::all();

        return response()->json([
            "data" => $roles,
            "code" => $this->successStatus,
            "message" => "Roles.",
        ]);
    }

    public function getDesignationForSelect(Request $request)
    {
        $roles = Designation::all();

        return response()->json([
            "data" => $roles,
            "code" => $this->successStatus,
            "message" => "Roles.",
        ]);
    }

    public function getDBVariable(Request $request)
    {
        $type_id = $request->type_id;
        $details = DbVariable::where('id', $type_id)
            ->with('details')
            ->get();

        return response()->json([
            "data" => $details,
            "code" => $this->successStatus,
            "message" => "System Variable.",
        ]);
    }

    public function getSystemVariable(Request $request)
    {
        $db_variable = DbVariable::all();

        return response()->json([
            "data" => $db_variable,
            "code" => $this->successStatus,
            "message" => "System Variable.",
        ]);

    }

    public function fetchOptionById(Request $request)
    {
        $id = $request->id;
        $db_variable = DbVariableDetail::find($id);

        return response()->json([
            "data" => $db_variable,
            "code" => $this->successStatus,
            "message" => "System Variable.",
        ]);

    }

    public function createSytemVarable(Request $request)
    {

        $message = "";
        $rules = [
            "options" => "required",

        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,

            ], $this->validationStatus);
        }
        $systemVariableCreate = null;
        $id = $request->id;
        if ($id == null) {
            $systemVariableCreate = DbVariableDetail::create($request->all());
            $message = "System Variable Created.";
        } else {
            $systemVariableCreate = DbVariableDetail::where(['id' => $id])->update([
                'options' => $request->options,
            ]);

            $message = "Syetem Variable Updated.";
        }

        return response()->json([
            "data" => $systemVariableCreate,
            "code" => $this->successStatus,
            "message" => $message,
        ]);

    }

    public function getNotifications(Request $request)
    {

        $user_id = $request->user_id;
        $all = $request->all;

        $notifications = Notification::where('to', $user_id);

        if ($all) {
            $notifications = $notifications->take(4);
        }
        $notifications = $notifications
            ->orderBy('id', 'DESC')
            ->get();

        return response()->json([
            "data" => $notifications,
            "code" => $this->successStatus,
            "message" => "Notifications.",
        ]);
    }

    public function readNotifications(Request $request)
    {
        $id = $request->id;

        $notifications = Notification::where('id', $id)
            ->update([
                'is_read' => "1",
            ]);

        return response()->json([
            "data" => $notifications,
            "code" => $this->successStatus,
            "message" => "Marked Read.",
        ]);
    }
}
