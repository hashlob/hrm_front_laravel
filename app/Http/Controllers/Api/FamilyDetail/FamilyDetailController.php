<?php

namespace App\Http\Controllers\Api\FamilyDetail;

use App\FamilyDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FamilyDetailController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public $uploadPath = "uploads/employees/";

    public function create(Request $request)
    {

        $rules = [
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $userId = $request->user_id;
        $id = $request->id;
        $familyDetails = FamilyDetail::where(['user_id' => $userId, 'id' => $id])->get();

        $verification_document = $request->file('verification_document');
        $verification_document_name = null;
        $uploat_to = $this->uploadPath . $request->employee_number . "/docs";

        if ($verification_document != null) {
            $verification_document_name = $verification_document->getClientOriginalName();
            $verification_document->move($uploat_to, $verification_document_name);
        }

        if (count($familyDetails) == 0) {
            $FamilyDetail = new FamilyDetail;
            $FamilyDetail->name = $request->name;
            $FamilyDetail->cnic = $request->cnic;
            $FamilyDetail->nationality = $request->nationality;
            $FamilyDetail->profession = $request->profession;
            $FamilyDetail->user_id = $request->user_id;
            $FamilyDetail->contact = $request->contact;
            $FamilyDetail->relation = $request->relation;
            $FamilyDetail->address = $request->address;
            $FamilyDetail->blood_group = $request->blood_group;
            $FamilyDetail->verification_document = $verification_document_name;

            $FamilyDetail->save();

            return response()->json([
                "data" => $FamilyDetail,
                "code" => $this->successStatus,
                "message" => "Family Details Created.",
            ], $this->successStatus);
        } else {

            $FamilyDetail = FamilyDetail::where(['user_id' => $userId, 'id' => $id])->update([
                'name' => $request->name,
                'cnic' => $request->cnic,
                'nationality' => $request->nationality,
                'profession' => $request->profession,
                'relation' => $request->relation,
                'address' => $request->address,
                'blood_group' => $request->blood_group,
                'contact' => $request->contact,
                'verification_document' => $verification_document_name,
            ]);

            return response()->json([
                "data" => $FamilyDetail,
                "code" => $this->successStatus,
                "message" => "Family Details Updated.",
            ], $this->successStatus);
        }

    }

    public function getUsersFamilyDetail(Request $request)
    {
        $userId = $request->userId;
        $FamilyDetails = FamilyDetail::where('user_id', $userId)->get();

        return response()->json([
            "data" => $FamilyDetails,
            "code" => $this->successStatus,
            "message" => "Family Details.",
        ], $this->successStatus);

    }

    public function getFamilyDetailById(Request $request)
    {
        $id = $request->id;
        $FamilyDetails = FamilyDetail::find($id)->first();

        return response()->json([
            "data" => $FamilyDetails,
            "code" => $this->successStatus,
            "message" => "Family Details.",
        ], $this->successStatus);
    }

}
