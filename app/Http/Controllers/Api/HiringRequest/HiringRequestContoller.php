<?php

namespace App\Http\Controllers\Api\HiringRequest;

use App\HiringRequest;
use App\Http\Controllers\Controller;
use App\JobPost;
use Illuminate\Http\Request;

class HiringRequestContoller extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {
        $id = $request->id;
        $job_title = $request->job_title;
        $job_description = $request->job_description;
        $reason = $request->reason;
        $date = $request->date;
        $department_id = $request->department_id;
        $manager_id = $request->manager_id;
        $hiring_request = null;
        $message = null;

        if ($id == null) {
            $hiring_request = HiringRequest::create([
                'job_title' => $job_title,
                'job_description' => $job_description,
                'reason' => $reason,
                'date' => $date,
                'department_id' => $department_id,
                'manager_id' => $manager_id,
            ]);

            $message = "Hiring request created.";
        } else {
            $hiring_request = HiringRequest::where('id', $id)
                ->update([
                    'job_title' => $job_title,
                    'job_description' => $job_description,
                    'reason' => $reason,
                    'date' => $date,
                ]);

            $message = "Hiring request updated.";
        }

        return response()->json([
            "data" => $hiring_request,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getHiringRequests(Request $request)
    {
        $role_id = $request->role_id;
        $department_id = $request->department_id;
        $manager_id = $request->manager_id;
        $hiring_requests = null;

        $hiring_requests = HiringRequest::with('manager', 'department');

        if ($role_id == 2) {
            // $hiring_requests = $hiring_requests->get();
        } else {
            $hiring_requests = $hiring_requests->where([
                'department_id' => $department_id,
                'manager_id' => $manager_id,
            ]);

        }

        $hiring_requests = $hiring_requests->get();

        return response()->json([
            "data" => $hiring_requests,
            "code" => $this->successStatus,
            "message" => "Hiring requests.",
        ], $this->successStatus);
    }

    public function processHiringRequest(Request $request)
    {
        $id = $request->id;
        $manager_id = $request->manager_id;
        $from = $request->from;
        $create_job = $request->create_job;
        $to = $request->to;
        $status = $request->status;
        $result = null;
        $message = null;

        $hiring_request = HiringRequest::where('id', $id)
            ->get()
            ->first();

        $job_title = $hiring_request->job_title;
        $job_description = $hiring_request->job_description;
        $requested_by = $hiring_request->manager_id;
        $department_id = $hiring_request->department_id;

        if ($status == 0) {
            $result = HiringRequest::where('id', $id)
                ->update([
                    'status' => $status,
                ]);
            $message = "Hiring request rejected.";
        } else {
            $result = HiringRequest::where('id', $id)
                ->update([
                    'status' => $status,
                ]);

            if ($create_job == 1) {

                $result = JobPost::create([
                    'job_title' => $job_title,
                    'job_description' => $job_description,
                    'from' => $from,
                    'to' => $to,
                    'manager_id' => $manager_id,
                    'requested_by' => $requested_by,
                    'department_id' => $department_id,
                ]);

                $message = "Job request Created.";

            } else {
                $message = "Hiring request Approved.";
            }

        }

        return response()->json([
            "data" => $result,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function fetchHiringById(Request $request)
    {
        $id = $request->id;

        $hiring_request = HiringRequest::where('id', $id)
            ->with('manager', 'department')
            ->get()->first();

        return response()->json([
            "data" => $hiring_request,
            "code" => $this->successStatus,
            "message" => "hiring detail.",
        ], $this->successStatus);
    }
}
