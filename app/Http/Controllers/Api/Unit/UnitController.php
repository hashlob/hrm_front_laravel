<?php

namespace App\Http\Controllers\Api\Unit;

use App\Http\Controllers\Controller;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public function create(Request $request)
    {

        $unit = null;
        $message = null;
        $id = $request->id;
        if ($id == null) {
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:units',
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {

                return response()->json([
                    'errors' => $validator->errors(),
                    'code' => $this->validationStatus,
                    'message' => $this->validationMessage,
                ], $this->validationStatus);
            }   
            $unit = Unit::create($request->all());
        } else {
            $unit = Unit::where('id', $id)
                ->update([
                    'name' => $request->name,
                    'code' => $request->code,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'prefix' => $request->prefix,
                    'type' => $request->type,
                    'shift_start' => $request->shift_start,
                    'shift_end' => $request->shift_end,
                    'week_start' => $request->week_start,
                    'address' => $request->address,
                ]);
        }

        return response()->json([
            "data" => $unit,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
        try {
            $units = Unit::with('unitType')
                ->get();

            return response()->json([
                "data" => $units,
                "code" => $this->successStatus,
                "message" => "Units.",
            ], $this->successStatus);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $this->unauthorisedMessage,
                'message' => $this->errorMessage,
                "code" => $this->unauthorisedStatus,
            ], $this->unauthorisedStatus);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $unit = Unit::destroy($id);

        return response()->json([
            "data" => $unit,
            "code" => $this->successStatus,
            "message" => "Unit Deleted.",
        ], $this->successStatus);
    }

    public function geUnitById(Request $request)
    {
        $id = $request->id;
        $unit = Unit::find($id);

        return response()->json([
            "data" => $unit,
            "code" => $this->successStatus,
            "message" => "Unit.",
        ], $this->successStatus);
    }
}
