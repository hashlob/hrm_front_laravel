<?php

namespace App\Http\Controllers\Api\EmergencyContacts;

use App\EmergencyContactsDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmergencyContactsDetailController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public function create(Request $request)
    {

        $rules = [
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $userId = $request->user_id;
        $id = $request->id;
        $EmergencyContactsDetails = EmergencyContactsDetail::where(['user_id' => $userId, 'id' => $id])->get();

        if (count($EmergencyContactsDetails) == 0) {
            $EmergencyContactsDetail = new EmergencyContactsDetail;
            $EmergencyContactsDetail->name = $request->name;
            $EmergencyContactsDetail->cnic = $request->cnic;
            $EmergencyContactsDetail->user_id = $request->user_id;
            $EmergencyContactsDetail->contact = $request->contact;
            $EmergencyContactsDetail->relation = $request->relation;
            $EmergencyContactsDetail->address = $request->address;

            $EmergencyContactsDetail->save();

            return response()->json([
                "data" => $EmergencyContactsDetail,
                "code" => $this->successStatus,
                "message" => "Emergency Contacts Detail Created.",
            ], $this->successStatus);
        } else {

            $EmergencyContactsDetail = EmergencyContactsDetail::where(['user_id' => $userId, 'id' => $id])->update([
                'name' => $request->name,
                'cnic' => $request->cnic,
                'relation' => $request->relation,
                'address' => $request->address,
                'contact' => $request->contact,
            ]);

            return response()->json([
                "data" => $EmergencyContactsDetail,
                "code" => $this->successStatus,
                "message" => "Emergency Contacts Detail Updated.",
            ], $this->successStatus);
        }

    }

    public function getUsersEmergencyetail(Request $request)
    {
        $userId = $request->userId;
        $EmergencyContactsDetails = EmergencyContactsDetail::where('user_id', $userId)->get();

        return response()->json([
            "data" => $EmergencyContactsDetails,
            "code" => $this->successStatus,
            "message" => "Emergency Contacts Detail.",
        ], $this->successStatus);

    }

    public function fetchEmergencyDetailById(Request $request)
    {
        $id = $request->id;
        $EmergencyContactsDetails = EmergencyContactsDetail::find($id)->first();

        return response()->json([
            "data" => $EmergencyContactsDetails,
            "code" => $this->successStatus,
            "message" => "Emergency Contacts Detail.",
        ], $this->successStatus);
    }
}
