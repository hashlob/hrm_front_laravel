<?php

namespace App\Http\Controllers\Api\JobPost;

use App\CvBank;
use App\Department;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CvBankController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public $uploadPath = "uploads/Cvs/";

    public function upload(Request $request)
    {
        $date = $request->date;
        $department_id = $request->department_id;
        $cv = $request->file('cv');

        $uploat_to = $this->uploadPath . date('m-Y', strtotime($date));
        $original_name = $cv->getClientOriginalName();
        $filename = $this->random_string(5, $original_name);
        $cv->move($uploat_to, $filename);

        $cv_bank = CvBank::create([
            'path' => date('m-Y', strtotime($date)) . "/" . $filename,
            'department_id' => $department_id,
            'next_approver' => 2,
            'date' => $date,
            'name' => $original_name,
        ]);

        return response()->json([
            "data" => $cv_bank,
            "message" => "CV Uploaded.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function getAllCvs(Request $request)
    {

        $status = $request->status;
        $allCvs = CvBank::with('department');

        if ($status != null) {
            $allCvs = $allCvs->where('status', $status);
        }

        $allCvs = $allCvs->get();

        return response()->json([
            "data" => $allCvs,
            "message" => "CV Uploaded.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function changeStatus(Request $request)
    {

        $id = $request->id;
        $status = $request->status;
        $user_id = $request->user_id;
        $department_id = $request->department_id;

        $department = Department::where('id', $department_id)
            ->get()
            ->first();
        $manager_id = null;

        if ($department->manager_id != null) {
            $manager_id = $department->manager_id;
        }

        $next_approver = null;

        if ($user_id == 2) {
            $status = "2";
            $next_approver = $manager_id;
        } else {
            $next_approver = 2;
        }

        $cvs = CvBank::where('id', $id)
            ->update([
                'status' => $status,
                'next_approver' => $next_approver,
            ]);

        return response()->json([
            "data" => $cvs,
            "message" => "CV Uploaded.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function downloadCv(Request $request)
    {
        $path = $request->path;
        $file = $this->uploadPath . $path;

        return response()->download(public_path($file));

    }

    public function random_string($length, $original_name)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key . "_" . $original_name;
    }
}
