<?php

namespace App\Http\Controllers\Api\JobPost;

use App\Http\Controllers\Controller;
use App\InterviewLineup;
use App\InterviewPanel;
use Illuminate\Http\Request;

class InterviewController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {
        $interview_date = $request->interview_date;
        $interview_time = $request->interview_time;
        $test_details = $request->test_details;
        $interview_details = $request->interview_details;
        $cv_id = $request->cv_id;
        $interview_panel = $request->interview_panel;
        $id = $request->id;
        $message = null;

        $check_interview = InterviewLineup::where('cv_id', $cv_id)
            ->get();

        if (sizeof($check_interview) > 0) {
            return response()->json([
                "data" => [],
                "message" => "Interview already lined up",
                "code" => $this->validationStatus,
            ], $this->validationStatus);
        }

        if ($id == null) {
            $interview_lineup = InterviewLineup::create([
                'interview_date' => $interview_date,
                'interview_time' => $interview_time,
                'test_details' => $test_details,
                'interview_details' => $interview_details,
                'cv_id' => $cv_id,
            ]);

            foreach ($interview_panel as $key => $value) {
                InterviewPanel::create([
                    'interview_lineup_id' => $interview_lineup->id,
                    'interviewer_id' => $value,
                ]);
            }
            $message = "Interview lined up.";
        } else {
            $interview_lineup = InterviewLineup::where('id', $id)
                ->update([
                    'interview_date' => $interview_date,
                    'interview_time' => $interview_time,
                    'test_details' => $test_details,
                    'interview_details' => $interview_details,
                ]);

            InterviewPanel::where('interview_lineup_id', $id)->delete();
            foreach ($interview_panel as $key => $value) {
                InterviewPanel::create([
                    'interview_lineup_id' => $id,
                    'interviewer_id' => $value,
                ]);
            }
            $message = "Interview updated.";
        }

        return response()->json([
            "data" => $interview_lineup,
            "message" => $message,
            "code" => $this->successStatus,
        ], $this->successStatus);

    }

    public function getAllInterviews(Request $request)
    {
        $allInterviews = InterviewLineup::with('cv', 'panel.interviewers');

        $allInterviews = $allInterviews->get();

        return response()->json([
            "data" => $allInterviews,
            "message" => "Interview lined up.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function fetchInterviewById(Request $request)
    {

        $id = $request->id;
        $Interview = InterviewLineup::where('id', $id)
            ->with('cv', 'panel.interviewers')
            ->get()->first();

        return response()->json([
            "data" => $Interview,
            "message" => "Interview lined up.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $interviews = InterviewLineup::where('id', $id)
            ->update([
                'status' => $status,
            ]);

        return response()->json([
            "data" => $interviews,
            "message" => "Status updated.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }
}
