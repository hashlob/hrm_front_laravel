<?php

namespace App\Http\Controllers\Api\JobPost;

use App\Http\Controllers\Controller;
use App\JobPost;
use Illuminate\Http\Request;

class JobPostContoller extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        $job_title = $request->job_title;
        $job_description = $request->job_description;
        $from = $request->from;
        $to = $request->to;
        $manager_id = $request->manager_id;
        $department_id = $request->department_id;
        $requested_by = $request->requested_by;
        $id = $request->id;
        $job_post = null;

        if ($id == null) {
            $job_post = JobPost::create([
                'job_title' => $job_title,
                'job_description' => $job_description,
                'from' => $from,
                'to' => $to,
                'manager_id' => $manager_id,
                'department_id' => $department_id,
                'requested_by' => $requested_by,

            ]);

            $message = "Job request Created.";
        } else {
            $job_post = JobPost::where('id', $id)
                ->update([
                    'job_title' => $job_title,
                    'job_description' => $job_description,
                    'from' => $from,
                    'to' => $to,
                ]);

            $message = "Job request updated.";
        }

        return response()->json([
            "data" => $job_post,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getJobPosts(Request $request)
    {

        $status = $request->status;
        $job_posts = JobPost::with('manager', 'department');

        if ($status != null) {
            $job_posts = $job_posts->where('status', $status);
        }

        $job_posts = $job_posts->get();

        return response()->json([
            "data" => $job_posts,
            "code" => $this->successStatus,
            "message" => "Job Posts",
        ], $this->successStatus);
    }

    public function changeStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $job_posts = JobPost::where('id', $id)
            ->update([
                'status' => $status,
            ]);

        return response()->json([
            "data" => $job_posts,
            "code" => $this->successStatus,
            "message" => "Status Updated.",
        ], $this->successStatus);
    }

    public function getJobPostById(Request $request)
    {
        $id = $request->id;
        $job_post = JobPost::where('id', $id)
            ->get()
            ->first();
            
        return response()->json([
            "data" => $job_post,
            "code" => $this->successStatus,
            "message" => "Job Post.",
        ], $this->successStatus);
    }
}
