<?php

namespace App\Http\Controllers\Api\Zone;

use App\Http\Controllers\Controller;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ZoneController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        $rules = [
            "name" => "required|unique:zones",
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'data' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }

        $id = $request->id;
        $message = "";
        if ($id == null) {
            $zone = Zone::create($request->all());
            $message = "Zone created.";
        } else {
            $zone = Zone::where('id', $id)
                ->update([
                    'name' => $request->name,
                    'manager_id' => $request->manager_id,
                ]);
            $message = "Zone updated.";

        }

        return response()->json([
            "data" => $zone,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
        $zones = Zone::with('manager')
            ->get();

        return response()->json([
            "data" => $zones,
            "code" => $this->successStatus,
            "message" => "zones",
        ], $this->successStatus);
    }

    public function changeZoneStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $zone = Zone::where('id', $id)->update([
            'is_active' => $status,
        ]);
        $message = "Status changed to ";
        $status = $status == 0 ? "Inactive" : "Active";

        return response()->json([
            "data" => $zone,
            "code" => $this->successStatus,
            "message" => $message . $status,
            'zone_id' => $id,
        ]);
    }

    public function fetchZoneById(Request $request)
    {
        $id = $request->id;

        $zone = Zone::where('id', $id)
            ->get()->first();
        return response()->json([
            "data" => $zone,
            "code" => $this->successStatus,
            "message" => 'Zone',
        ]);
    }
}
