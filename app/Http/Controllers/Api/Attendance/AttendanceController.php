<?php

namespace App\Http\Controllers\Api\Attendance;

use App\Attendance;
use App\EmploymentDetail;
use App\Http\Controllers\Controller;
use App\LeaveRequest;
use App\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function markAttendance(Request $request)
    {
        $time_in = $request->time_in;
        $time_out = $request->time_out;
        $attedance_type = $request->status ? 1 : 0;
        $machine_id = $request->machine_id;
        $date = $request->date;
        $user_id = $request->user_id;
        $id = $request->id;
        $branch_id = '';
        $department_id = '';

        $employment_details = EmploymentDetail::where('user_id', $user_id)->get()->first();
        if ($employment_details && ($employment_details->shift_timein || $employment_details->shift_timeout)) {
            $employee_shift_timein = $employment_details->shift_timein;
            $employee_shift_timeout = $employment_details->shift_timeout;
        } else {
            return response()->json([
                'data' => [],
                'message' => "Employee shift timing not entered.",
                "code" => $this->validationStatus,
            ], $this->validationStatus);
        }

        if ($employment_details && ($employment_details->department_id == null || $employment_details->unit == null)) {
            return response()->json([
                'data' => [],
                'message' => "Employee Department or unit not assigned.",
                "code" => $this->validationStatus,
            ], $this->validationStatus);
        } else {
            $branch_id = $employment_details->unit_id;
            $department_id = $employment_details->department_id;
        }

        $service_hours = '0';
        $extra_hours = '0';
        if ($time_in && $time_out) {
            $service_hours = $this->calculateServiceHours($time_in, $time_out);
            $extra_hours = $this->calculateExtraHours($time_in, $time_out, $employee_shift_timein, $employee_shift_timeout);
        } else {
            $time_in = "00:00";
            $time_out = "00:00";
        }

        $attendance = null;
        $message = "";
        $attendance_check = Attendance::where(['date' => $date, 'user_id' => $user_id])->get();

        if (sizeof($attendance_check) == 0) {

            $attendance = Attendance::create([
                'time_in' => $time_in,
                'time_out' => $time_out,
                'attedance_type' => $attedance_type,
                'machine_id' => $machine_id,
                'branch_id' => $branch_id,
                'user_id' => $user_id,
                'service_hours' => $service_hours,
                'extra_hours' => $extra_hours,
                'date' => $date,
                'department_id' => $department_id,
            ]);
            $message = "Attendance marked.";
        } else {
            $attendance = Attendance::where(['date' => $date, 'user_id' => $user_id])
                ->update([
                    'time_in' => $time_in,
                    'time_out' => $time_out,
                    'date' => $date,
                    'attedance_type' => $attedance_type,
                    'machine_id' => $machine_id,
                    'branch_id' => $branch_id,
                    'user_id' => $user_id,
                    'service_hours' => $service_hours,
                    'extra_hours' => $extra_hours,
                    'department_id' => $department_id,
                ]);
            $message = "Attendance updated.";
        }

        return response()->json([
            'data' => $attendance,
            'message' => $message,
            "code" => $this->successStatus,
        ], $this->successStatus);

    }

    public function getAttendanceReport(Request $request)
    {

        $month = $request->month;
        $year = $request->year;
        $date = $request->date;
        $dateStart = $request->dateStart;
        $dateEnd = $request->dateEnd;
        $message = "";
        $report_data = [];

        $days = cal_days_in_month(0, $month, $year);
        $users = User::all();

        $leave_requests = LeaveRequest::where('from', '>=', $dateStart)
            ->where('to', '<=', $dateEnd)
            ->where('approved_status', "1")
            ->get();

        $attendance_report = Attendance::whereRaw('month(date) = ' . $month)
            ->get();

        foreach ($users as $key => $user) {
            $data = [];
            $data['user_id'] = $user->id;
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['attendance_array'] = [];

            for ($i = 1; $i <= $days; $i++) {

                $time = mktime(12, 0, 0, $month, $i, $year);
                $date = date('Y-m-d', $time);

                $attendance_obj = [];
                $attendance_found = false;
                $leave_found = false;
                foreach ($attendance_report as $key => $value) {
                    if ($value->user_id == $data['user_id'] && $value->date == $date) {
                        $attendance_obj = $value;
                        $attendance_found = true;
                        break;
                    }
                }
                $date_to = '';
                $date_from = '';
                foreach ($leave_requests as $key => $value) {
                    if ($value->applied_by == $data['user_id']) {
                        $date_to = $value->to;
                        $date_from = $value->from;
                        $leave_found = true;
                        break;
                    }
                }

                if ($attendance_found) {

                    array_push($data['attendance_array'], $attendance_obj);

                } elseif ($leave_found) {

                    $dates = $this->getDatesFromRange($date_from, $date_to);

                    if (in_array($date, $dates)) {
                        array_push($data['attendance_array'], [
                            'attedance_type' => 3,
                            'date' => $date,
                        ]);
                    } else {
                        array_push($data['attendance_array'], [
                            'attedance_type' => 2,
                            'date' => $date,
                        ]);
                    }

                } else {
                    array_push($data['attendance_array'], [
                        'attedance_type' => 2,
                        'date' => $date,
                    ]);
                }

            }
            array_push($report_data, $data);

        }

        return response()->json([
            'data' => $report_data,
            'message' => $message,
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function markAttendanceFromMachine(Request $request)
    {

        $file = $request->file('file');
        $data = file_get_contents($file);
        $data = json_decode($data);

        foreach ($data as $key => $value) {
            $day = $value->dayId;
            $month = $value->monthId;
            $year = $value->yearId;
            $hour = $value->hourId;
            $minute = $value->minuteId;
            $second = $value->secondId;
            $inOutMode = $value->inOutId;
            $employeeNumber = $value->employeeNumber;
            $employeeNumberPF = "PF-$employeeNumber";

            $attendanceDateTime = mktime($hour, $minute, $second, $month, $day, $year);
            $attendanceDate = date('Y-m-d', $attendanceDateTime);
            $attendanceTime = date('H:i', $attendanceDateTime);

            $employeeDetails = USER::with('employmentDetails')->where('employee_number', $employeeNumberPF)->get()->first();

            if ($employeeDetails != null) {

                $employeeId = $employeeDetails->id;
                $department_id = $employeeDetails->employmentDetails != null ? $employeeDetails->employmentDetails->department_id : null;
                $branch_id = $employeeDetails->employmentDetails != null ? $employeeDetails->employmentDetails->unit_id : null;
                $employee_shift_timein = $employeeDetails->employmentDetails != null ? $employeeDetails->employmentDetails->shift_timein : null;
                $employee_shift_timeout = $employeeDetails->employmentDetails != null ? $employeeDetails->employmentDetails->shift_timeout : null;
                $service_hours = '0';
                $extra_hours = '0';

                $attendanceCheck = Attendance::where([
                    'date' => $attendanceDate,
                    'user_id' => $employeeId,
                ])->get();
                if (sizeof($attendanceCheck) == 0) {
                    Attendance::create([
                        "time_in" => $attendanceTime,
                        "time_out" => "00:00:00",
                        "date" => $attendanceDate,
                        "attedance_type" => "1",
                        "service_hours" => $service_hours,
                        "extra_hours" => $extra_hours,
                        "department_id" => $department_id,
                        "branch_id" => $branch_id,
                        "user_id" => $employeeId,
                    ]);
                } else {
                    if ($inOutMode == "1") {

                        $time_in = $attendanceCheck[0]->time_in;
                        $time_out = $attendanceTime;

                        if ($time_out != "00:00:00" && $employee_shift_timein && $employee_shift_timeout) {
                            $service_hours = $this->calculateServiceHours($time_in, $time_out);
                            $extra_hours = $this->calculateExtraHours($time_in, $time_out, $employee_shift_timein, $employee_shift_timeout);
                        }

                        Attendance::where([
                            'date' => $attendanceDate,
                            'user_id' => $employeeId,
                        ])
                            ->update([
                                "service_hours" => $service_hours,
                                "extra_hours" => $extra_hours,
                                "time_out" => $attendanceTime,
                            ]);
                    } else {
                        Attendance::where([
                            'date' => $attendanceDate,
                            'user_id' => $employeeId,
                        ])
                            ->update([
                                "time_in" => $attendanceTime,
                            ]);
                    }
                }

            }

        }
        return response()->json([
            "data" => $data,
            "code" => $this->successStatus,
            "message" => "data saved",
        ], $this->successStatus);
    }

    public function calculateServiceHours($time_in, $time_out)
    {
        $time_in = strtotime($time_in);
        $time_out = strtotime($time_out);
        $service_hours = ($time_out - $time_in) / 60;
        return round($service_hours);
    }

    public function getDatesFromRange($start, $end, $format = 'Y-m-d')
    {
        $array = array();
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        foreach ($period as $date) {
            $array[] = $date->format($format);
        }

        return $array;
    }

    public function calculateExtraHours($time_in, $time_out, $employee_shift_timein, $employee_shift_timeout)
    {
        $time_in = strtotime($time_in);
        $time_out = strtotime($time_out);
        $employee_shift_timein = strtotime($employee_shift_timein);
        $employee_shift_timeout = strtotime($employee_shift_timeout);
        $early = $time_in - $employee_shift_timein;
        $after = $employee_shift_timeout - $time_out;

        if ($early >= 0) {
            $early = 0;
        } else {
            $early = abs($early);
        }

        if ($after >= 0) {
            $after = 0;
        } else {
            $after = abs($after);
        }

        $total = $early + $after;
        return ($total) / 60;
    }
}
