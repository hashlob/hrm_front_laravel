<?php

namespace App\Http\Controllers\Api\HiringDetail;

use App\HiringDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HiringDetailController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public function create(Request $request)
    {

        $rules = [
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $userId = $request->user_id;
        $hiringDetails = HiringDetail::where('user_id', $userId)->get();

        if (count($hiringDetails) == 0) {
            $hiringDetail = HiringDetail::create($request->all());

            return response()->json([
                "data" => $hiringDetail,
                "code" => $this->successStatus,
                "message" => "Hiring Details Created.",
            ], $this->successStatus);
        } else {
           
            $hiringDetail = HiringDetail::where('user_id', $userId)->update([
                'joining_date' => $request->joining_date ,
                'probation_period' => $request->probation_period,
                'test_details' => $request->test_details,
                'training_details' => $request->training_details
            ]);

            return response()->json([
                "data" => $hiringDetail,
                "code" => $this->successStatus,
                "message" => "Hiring Details Updated.",
            ], $this->successStatus);
        }

    }

    public function getUsersHiringDetail(Request $request)
    {
        $userId = $request->userId;
        $hiringDetails = HiringDetail::where('user_id', $userId)->get();

        return response()->json([
            "data" => $hiringDetails,
            "code" => $this->successStatus,
            "message" => "Hiring Details.",
        ], $this->successStatus);

    }
}
