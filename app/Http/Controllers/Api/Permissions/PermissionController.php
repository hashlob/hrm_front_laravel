<?php

namespace App\Http\Controllers\Api\Permissions;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use App\RolePermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        $rules = [
            "name" => "required",
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'data' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }

        $id = $request->id;
        $message = "";
        if ($id == null) {
            $permission = Permission::create($request->all());
            $message = "Permission created.";
        } else {
            $permission = Permission::where('id', $id)
                ->update([
                    'name' => $request->name,
                ]);
            $message = "Permission updated.";

        }

        return response()->json([
            "data" => $permission,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function addRolePermission(Request $request)
    {
        $role_id = $request->role_id;
        $permission_id = $request->permission_id;
        $permissions = null;
        $message = null;

        $checkRolePermission = RolePermission::where([
            'role_id' => $role_id,
            'permission_id' => $permission_id,
        ])
            ->get();

        if (sizeof($checkRolePermission) == 0) {
            $permissions = RolePermission::create([
                'role_id' => $role_id,
                'permission_id' => $permission_id,
            ]);

            $message = "Permission for role added.";
        } else {
            $permissions = RolePermission::where([
                'role_id' => $role_id,
                'permission_id' => $permission_id,
            ])
                ->update([
                    'role_id' => $role_id,
                    'permission_id' => $permission_id,
                ]);

            $message = "Permission for role updated.";

        }
        return response()->json([
            "data" => $permissions,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {

        $permissions = Permission::all();

        return response()->json([
            "data" => $permissions,
            "code" => $this->successStatus,
            "message" => "Permissions.",
        ], $this->successStatus);
    }

    public function getPermissionByRole(Request $request)
    {
        $role_id = $request->role_id;

        $permissions = Role::where('id', $role_id)
            ->with('permissions')
            ->get();

        return response()->json([
            "data" => $permissions,
            "code" => $this->successStatus,
            "message" => "Permissions by role.",
        ], $this->successStatus);
    }

    public function getPermissionById(Request $request)
    {
        $id = $request->id;

        $permission = Permission::where('id', $id)
            ->get()
            ->first();

        return response()->json([
            "data" => $permission,
            "code" => $this->successStatus,
            "message" => "Permission by Id.",
        ], $this->successStatus);
    }

    public function delete(Request $request)
    {
        $permission_id = $request->permission_id;
        $role_id = $request->role_id;

        $permission = RolePermission::where([
            'permission_id' => $permission_id,
            'role_id' => $role_id,
        ])->delete();

        return response()->json([
            "data" => $permission,
            "code" => $this->successStatus,
            "message" => "Permission Removed.",
        ], $this->successStatus);
    }
}
