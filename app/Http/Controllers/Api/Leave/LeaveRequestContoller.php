<?php

namespace App\Http\Controllers\Api\Leave;

use App\EmploymentDetail;
use App\Http\Controllers\Controller;
use App\LeaveRequest;
use App\RequestLine;
use App\UserLeaves;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LeaveRequestContoller extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";
    public $request_line_type = 10;

    public function applyForLeave(Request $request)
    {
        $rules = [
            "reason" => "required",
            "from" => "required",
            "to" => "required",
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'data' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }

        $user_id = $request->applied_by;
        $leave_type = $request->leave_type;
        $from = $request->from;
        $to = $request->to;
        $department_id = '';
        $reason = $request->reason;
        $checkDept = EmploymentDetail::where('user_id', $user_id)->get()->first();
        if ($checkDept == null) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Department Not assigned.",
            ], $this->validationStatus);
        } else if ($checkDept->department_id) {
            $department_id = $checkDept->department_id;
        } else {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Department Not assigned.",
            ], $this->validationStatus);
        }

        $requestLine = RequestLine::where([
            'department_id' => $department_id,
            'request_line_type' => $this->request_line_type,
        ])
            ->orderBy('order', 'ASC')
            ->get()->first();

        if ($requestLine == null) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "No request Line defined",
            ], $this->validationStatus);
        }

        $next_inline = $requestLine->manager_id;

        $totalLeavesApplied = $this->diffBetweenDays($from, $to);

        $userleaves = UserLeaves::where([
            'user_id' => $user_id,
            'leave_type_id' => $leave_type,
        ])->get()->first();

        if ($userleaves == null) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Leaves not assigned.",
            ], $this->validationStatus);
        }

        if ($userleaves != null && $userleaves->quantity < $totalLeavesApplied) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Leaves not available.",
            ], $this->validationStatus);
        }

        $leaveRequest = LeaveRequest::create([
            'leave_type' => $leave_type,
            'reason' => $reason,
            'from' => $from,
            'to' => $to,
            'department_id' => $department_id,
            'next_inline_id' => $next_inline,
            'applied_by' => $user_id,
        ]);

        return response()->json([
            "data" => $leaveRequest,
            "code" => $this->successStatus,
            "message" => "Leave Applied",
        ], $this->successStatus);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $leaveConfig = LeaveRequest::find($id);
        $leaveConfig->delete();
        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration Deleted",
        ], $this->successStatus);
    }

    public function getLeavesDetails(Request $request)
    {
        $user_id = $request->user_id;
        $department_id = $request->department_id;
        $searchConfig = [];
        $requestLineSearchConfig = [
            'manager_id' => $user_id,
            'request_line_type' => $this->request_line_type,
        ];
        $leaveConfig = LeaveRequest::with('leaveType', 'appliedBy', 'department', 'nextInline');

        if ($user_id != 2) {
            $requestLineSearchConfig['department_id'] = $department_id;
        }
        $requestLine = RequestLine::where($requestLineSearchConfig)
            ->orderBy('order', 'ASC')
            ->get();
        $departmentIds = [];
        if (sizeof($requestLine) == 0) {
            // $searchConfig['applied_by'] = $user_id;
            $leaveConfig = $leaveConfig->where('applied_by', $user_id);
        } else {

            foreach ($requestLine as $key => $value) {
                array_push($departmentIds, $value->department_id);
            }
            $searchConfig['department_id'] = $departmentIds;
            $leaveConfig = $leaveConfig->whereIn('department_id', $departmentIds);
        }

        $leaveConfig = $leaveConfig->get();
        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration",
        ], $this->successStatus);

    }

    public function processLeaveApproval(Request $request)
    {
        $department_id = $request->department_id;
        $manager_id = $request->manager_id;
        $leave_id = $request->leave_id;
        $approved_status = $request->approved_status;
        $status_message = $request->status_message;

        $leaveRequest = LeaveRequest::find($leave_id)->first();
        $from = $leaveRequest->from;
        $to = $leaveRequest->to;
        $leave_type = $leaveRequest->leave_type;
        $user_id = $leaveRequest->applied_by;

        $userleaves = UserLeaves::where([
            'user_id' => $user_id,
            'leave_type_id' => $leave_type,
        ])->get()->first();

        $totalLeavesApplied = $this->diffBetweenDays($from, $to);

        if ($userleaves->quantity < $totalLeavesApplied) {
            return response()->json([
                "data" => [],
                "code" => $this->validationStatus,
                "message" => "Leave not available.",
            ], $this->validationStatus);
        }

        $leaveConfig = null;
        $requestLineSearch = [
            'request_line_type' => $this->request_line_type,
        ];
        $requestLineManagerSearch = [
            'manager_id' => $manager_id,
            'request_line_type' => $this->request_line_type,
        ];

        if ($manager_id != 2) {
            $requestLineSearch['department_id'] = $department_id;
            $requestLineManagerSearch['department_id'] = $department_id;
        }

        $requestLine = RequestLine::where($requestLineSearch)
            ->orderBy('order', 'ASC')
            ->get();

        $requestLineManager = RequestLine::where($requestLineManagerSearch)
            ->orderBy('order', 'ASC')
            ->get();

        $requestLineManagerCount = $requestLineManager->count();

        if ($requestLineManagerCount == 0) {
            return response()->json([
                "data" => $requestLineManager,
                "code" => $this->unauthorisedStatus,
                "message" => "Action not allowed.",
            ], $this->unauthorisedStatus);
        }

        if ($approved_status == 0) {
            $leaveConfig = LeaveRequest::where('id', $leave_id)
                ->update([
                    'status_message' => $status_message,
                    'approved_status' => "0",
                    'rejected_by' => $manager_id,
                ]);
        } else {

            $index = 0;

            foreach ($requestLine as $key => $line) {
                if ($manager_id == $line->manager_id) {
                    $index = $key;
                    break;
                }
            }
            if ($leaveRequest->next_inline < $requestLine->count()) {

                $message = "";
                if (($index + 1) == $requestLine->count()) {

                    $leaveConfig = LeaveRequest::where('id', $leave_id)
                        ->update([
                            'status_message' => "Approved by " . $requestLineManager[0]->line_name,
                            'next_inline' => ($index + 1),
                            'approved_status' => "1",
                            'next_inline_id' => null,
                        ]);
                    $message = "Leave Approved.";

                    $remainingLeaves = $userleaves->quantity - $totalLeavesApplied;

                    UserLeaves::where([
                        'user_id' => $user_id,
                        'leave_type_id' => $leave_type,
                    ])->update([
                        'quantity' => $remainingLeaves,
                    ]);

                } else {
                    $leaveConfig = LeaveRequest::where('id', $leave_id)
                        ->update([
                            'status_message' => "Approved by " . $requestLineManager[0]->line_name,
                            'next_inline' => ($index + 1),
                            'approved_status' => "3",
                            'next_inline_id' => $requestLine[$index + 1]->manager_id,

                        ]);
                    $message = "Leave Approval moved to next in line request.";
                }

            } else {
                return response()->json([
                    "data" => $leaveConfig,
                    "code" => $this->successStatus,
                    "message" => "Leave Already Approved.",
                ], $this->successStatus);
            }
            return response()->json([
                "data" => $leaveConfig,
                "code" => $this->successStatus,
                "message" => $message,
            ], $this->successStatus);

        }

        return response()->json([
            "data" => $leaveConfig,
            "code" => $this->successStatus,
            "message" => "Leave Configuration",
        ], $this->successStatus);
    }

    public function diffBetweenDays($from, $to)
    {
        $fromDate = strtotime($from);
        $toDate = strtotime($to);
        $total = $toDate - $fromDate;
        return $total / 86400;
    }
}
