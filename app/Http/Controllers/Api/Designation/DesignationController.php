<?php

namespace App\Http\Controllers\Api\Designation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Designation;
 
class DesignationController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public function create(Request $request)
    {

        $rules = [
            "title" => "required|unique:designations",
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }

        $designation = Designation::create($request->all());

        return response()->json([
            "data" => $designation,
            "code" => $this->successStatus,
            "message" => "Designation created.",
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
            $designations = Designation::all();

            return response()->json([
                "data" => $designations,
                "code" => $this->successStatus,
                "message" => "Designations.",
            ], $this->successStatus);
       
    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $designation = Designation::destroy($id);

        return response()->json([
            "data" => $designation,
            "code" => $this->successStatus,
            "message" => "Designation Deleted.",
        ], $this->successStatus);
    }

    public function geDesignationById(Request $request)
    {
        $id = $request->id;
        $designation = Designation::find($id);

        return response()->json([
            "data" => $designation,
            "code" => $this->successStatus,
            "message" => "Designation.",
        ], $this->successStatus);
    }
}
