<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiAuthController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if (Auth::attempt(['username' => request('username'), 'password' => request('password')])) {
            $userSystemDetails = Auth::User();
            $token = $userSystemDetails->createToken('MyApp')->accessToken;
            $user_id = $userSystemDetails->user_id;

            $user = User::where('id', $user_id)
                ->with("employmentDetails","systemDetail.role.permissions")->first();

            if ($user->is_active == 0) {
                return response()->json([
                    'error' => $this->unauthorisedMessage,
                    'message' => "Login not allowed. Inactive Status.",
                    "code" => $this->validationStatus,
                ]);
            }

            return response()->json([
                'token' => $token,
                "user" => $user,
                "userSystemDetails" => $userSystemDetails,
                "code" => $this->successStatus,
            ], $this->successStatus);
        } else {
            return response()->json([
                'error' => $this->unauthorisedMessage,
                'message' => "Invalid email or password.",
                "code" => $this->validationStatus,
            ]);
        }
    }

}
