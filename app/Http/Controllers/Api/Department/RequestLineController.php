<?php

namespace App\Http\Controllers\Api\Department;

use App\Department;
use App\Http\Controllers\Controller;
use App\RequestLine;
use Illuminate\Http\Request;

class RequestLineController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function create(Request $request)
    {

        $request_line_type = $request->request_line_type;
        $department_id = $request->department_id;
        $manager_id = $request->manager_id;
        
        $requestLine = null;

        $checkRequestLine = RequestLine::where([
            'request_line_type' => $request_line_type,
            'department_id' => $department_id,
            'manager_id' => $manager_id,
        ])->get();
        if (sizeof($checkRequestLine) == 0) {
            $requestLine = RequestLine::create($request->all());
            $message = "Request Line created.";
        } else {

            $requestLine = RequestLine::where([
                'request_line_type' => $request_line_type,
                'department_id' => $department_id,
                'manager_id' => $manager_id,
            ])
                ->update([
                    'manager_id' => $request->manager_id,
                    'order' => $request->order,
                    'line_name' => $request->line_name,
                ]);
            $message = "Request Line updated.";
        }

        return response()->json([
            "data" => $requestLine,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $requestLine = RequestLine::find($id);
        $requestLine->delete();
        return response()->json([
            "data" => $requestLine,
            "code" => $this->successStatus,
            "message" => "Request Line Deleted",
        ], $this->successStatus);
    }

    public function getForDepartment(Request $request)
    {
        $departId = $request->departId;
        $requestLineType = $request->requestLineType;
        $requestLine = RequestLine::where(['request_line_type' => $requestLineType, 'department_id' => $departId])
            ->with('department', 'manager')->get();

        return response()->json([
            "data" => $requestLine,
            "code" => $this->successStatus,
            "message" => "Request Line",
        ], $this->successStatus);

    }
}
