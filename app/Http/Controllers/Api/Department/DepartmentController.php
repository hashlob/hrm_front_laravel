<?php

namespace App\Http\Controllers\Api\Department;

use App\Department;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public function create(Request $request)
    {

        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $id = $request->id;
        if ($id == null) {
            $department = Department::create($request->all());
            $message = "Department Created.";
        } else {
            $department = Department::where('id', $id)
                ->update([
                    'name' => $request->name,
                    'code' => $request->code,
                    'prefix' => $request->prefix,
                    'manager_id' => $request->manager_id,
                    'parent_id' => $request->parent_id == "null" ? 0 : $request->parent_id,
                ]);
            $message = "Department Updated.";

        }

        return response()->json([
            "data" => $department,
            "code" => $this->successStatus,
            "message" => "Unit created.",
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
        $departments = Department::with('manager')->get();

        return response()->json([
            "data" => $departments,
            "code" => $this->successStatus,
            "message" => "Departments.",
        ], $this->successStatus);

    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $department = Department::destroy($id);

        return response()->json([
            "data" => $department,
            "code" => $this->successStatus,
            "message" => "Department Deleted.",
        ], $this->successStatus);
    }

    public function getDepartmentById(Request $request)
    {
        $id = $request->id;
        $department = Department::find($id);

        return response()->json([
            "data" => $department,
            "code" => $this->successStatus,
            "message" => "Department.",
        ], $this->successStatus);
    }
}
