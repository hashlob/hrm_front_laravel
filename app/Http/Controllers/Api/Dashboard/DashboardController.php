<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Attendance;
use App\Department;
use App\EmploymentDetail;
use App\Http\Controllers\Controller;
use App\LeaveRequest;
use App\TransferRequest;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
class DashboardController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function getDisplayCardsData(Request $request)
    {

        $fromDate = $request->fromDate;
        $toDate = $request->toDate;

        $employees = User::where('id', '<>', '1')->get()->toArray();
        $departments = Department::count();
        $branches = Unit::count();

        $totalActive = array_filter($employees, function ($empl) {
            if ($empl['is_active'] == "1") {
                return true;
            }

        });
        $totalInactive = array_filter($employees, function ($empl) {
            if ($empl['is_active'] == "0") {
                return true;
            }

        });
        $leaveRequests = LeaveRequest::where('approved_status', "<>", "1")
        // ->where('to', '<=', $toDate)
        // ->where('from', '>=', $fromDate)
            ->get()->count();
        $totalSalaries = EmploymentDetail::where('user_id', '<>', '1')
            ->sum('salary');
        $transferRequests = TransferRequest::where('status', '<>', '1')
            ->count();

        $data = array(
            'employees' => sizeof($employees),
            'departments' => $departments,
            'branches' => $branches,
            'leaveRequests' => $leaveRequests,
            'totalSalaries' => $totalSalaries,
            'totalActive' => sizeof($totalActive),
            'totalInactive' => sizeof($totalInactive),
            'transferRequests' => $transferRequests,
        );

        return response()->json([
            "data" => $data,
            "code" => $this->successStatus,
            "message" => "Result.",
        ], $this->successStatus);
    }

    public function getAttendaceCardsData(Request $request)
    {

        $date = $request->date;
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;

        // today

        $employees = User::where('id', '<>', '1')->count();

        $totalPresent = Attendance::where([
            'attedance_type' => "1",
            'date' => $date,
        ])->get()->count();

        $totalAbsent = Attendance::where([
            'attedance_type' => "0",
            'date' => $date,
        ])->get()->count();

        $totalOnLeave = LeaveRequest::where('from', '<=', $date)
            ->where('to', '>=', $date)
            ->where('approved_status', "1")
            ->get()->count();

        $notMarked = $employees - ($totalPresent + $totalAbsent + $totalOnLeave);

        // weekly

        $weeklyPresent = Attendance::select(DB::raw("(COUNT(*)) as count, week(date) as week"))
        ->whereBetween('date', [Carbon::parse($fromDate)->startOfWeek(),Carbon::parse($toDate)->endOfWeek()])
        ->where('attedance_type','1')
        ->groupBy('week')
        ->get();

        $weeklyAbsent = Attendance::select(DB::raw("(COUNT(*)) as count, week(date) as week"))
        ->whereBetween('date', [Carbon::parse($fromDate)->startOfWeek(),Carbon::parse($toDate)->endOfWeek()])
        ->where('attedance_type','0')
        ->groupBy('week')
        ->get();

        $data = array(
            'totalPresent' => $totalPresent,
            'totalAbsent' => $totalAbsent,
            'totalOnLeave' => $totalOnLeave,
            'totalEmployees' => $employees,
            'notMarked' => $notMarked,
            'weeklyPresent' => $weeklyPresent,
            'weeklyAbsent' => $weeklyAbsent,
        );

        return response()->json([
            "data" => $data,
            "code" => $this->successStatus,
            "message" => "Result.",
        ], $this->successStatus);
    }
}
