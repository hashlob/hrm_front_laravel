<?php

namespace App\Http\Controllers\Api\EducationDetail;

use App\EducationDetail;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EducationDetailController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public $uploadPath = "uploads/employees/";

    public function create(Request $request)
    {

        $rules = [
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $userId = $request->user_id;
        $id = $request->id;
        $emp_num = USER::where('id', $userId)->first();
        $educationDetails = EducationDetail::where(['id' => $id, 'user_id' => $userId])->get();

        $marksheet = $request->file('marksheet');
        $degree = $request->file('degree');
        $marksheet_name = null;
        $degree_name = null;
        $uploat_to = $this->uploadPath . $emp_num->employee_number . "/docs";

        if ($marksheet != null) {
            $marksheet_name = $marksheet->getClientOriginalName();
            $marksheet->move($uploat_to, $marksheet_name);
        }
        if ($degree != null) {
            $degree_name = $degree->getClientOriginalName();
            $degree->move($uploat_to, $degree_name);
        }

        if (count($educationDetails) == 0) {
            $educationDetail = new EducationDetail;
            $educationDetail->qualification = $request->qualification;
            $educationDetail->institute = $request->institute;
            $educationDetail->starting_date = $request->starting_date;
            $educationDetail->passing_date = $request->passing_date;
            $educationDetail->user_id = $request->user_id;
            $educationDetail->majors = $request->majors;
            $educationDetail->grade = $request->grade;
            $educationDetail->marksheet = $marksheet_name;
            $educationDetail->degree = $degree_name;

            $educationDetail->save();

            return response()->json([
                "data" => $educationDetail,
                "code" => $this->successStatus,
                "message" => "Education Details Created.",
            ], $this->successStatus);
        } else {

            $educationDetail = EducationDetail::where(['id' => $id, 'user_id' => $userId])->update([
                'qualification' => $request->qualification,
                'institute' => $request->institute,
                'starting_date' => $request->starting_date,
                'passing_date' => $request->passing_date,
                'majors' => $request->majors,
                'grade' => $request->grade,
                'marksheet' => $marksheet_name,
                'degree' => $degree_name,
            ]);

            return response()->json([
                "data" => $educationDetail,
                "code" => $this->successStatus,
                "message" => "Education Details Updated.",
            ], $this->successStatus);
        }

    }

    public function getUsersEducationDetail(Request $request)
    {
        $userId = $request->userId;
        $educationDetails = EducationDetail::where('user_id', $userId)->get();

        return response()->json([
            "data" => $educationDetails,
            "code" => $this->successStatus,
            "message" => "Education Details.",
        ], $this->successStatus);

    }

    public function fetchEducationDetailById(Request $request)
    {
        $id = $request->id;
        $educationDetails = EducationDetail::find($id)->first();

        return response()->json([
            "data" => $educationDetails,
            "code" => $this->successStatus,
            "message" => "Education Details.",
        ], $this->successStatus);
    }

    public function getMarksheet(Request $request)
    {
        $userId = $request->userId;
        $emp_num = USER::where('id', $userId)->first();

        // dd($emp_num);
        $file = $this->uploadPath . $emp_num->employee_number . "/docs" . "/" . $request->filename;

        return response()->download(public_path($file));
    }

    public function getDegree(Request $request)
    {
        $userId = $request->userId;
        $emp_num = USER::where('id', $userId)->first();

        $file = $this->uploadPath . $emp_num->employee_number . "/docs" . "/" . $request->filename;

        return response()->download(public_path($file));
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $educationDetail = EducationDetail::destroy($id);

        return response()->json([
            "data" => $educationDetail,
            "code" => $this->successStatus,
            "message" => "Deleted.",
        ], $this->successStatus);
    }
}
