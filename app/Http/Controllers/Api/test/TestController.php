<?php

namespace App\Http\Controllers\Api\test;

use App\Department;
use App\Designation;
use App\Http\Controllers\Controller;
use App\LeaveConfigurationDetails;
use App\User;
use App\UserLeaves;
use App\UserSystemDetail;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function readDepartmentFile(Request $request)
    {

        $path = "/home/hashlob/Desktop/mohsin/laravel/mongo_dump/departments.json";

        $Deptjson = json_decode(file_get_contents($path), true);
        foreach ($Deptjson as $key => $dept) {

            Department::create([
                'name' => $dept['name'],
                'code' => $dept['code'],
                'prefix' => $dept['prefix'],
            ]);

        }

        return response()->json([
            "data" => $Deptjson,
            "code" => $this->successStatus,
            "message" => "data saved",
        ], $this->successStatus);
    }

    public function readDesignationsFile(Request $request)
    {

        $path = "/home/hashlob/Desktop/mohsin/laravel/mongo_dump/designations.json";

        $designationJson = json_decode(file_get_contents($path), true);
        foreach ($designationJson as $key => $dept) {

            Designation::create([
                'title' => $dept['title'],
            ]);

        }

        return response()->json([
            "data" => $designationJson,
            "code" => $this->successStatus,
            "message" => "data saved",
        ], $this->successStatus);
    }

    public function readEmployeesFile(Request $request)
    {

        $path = "/home/hashlob/Desktop/mohsin/laravel/mongo_dump/employees.json";

        $leave_config = LeaveConfigurationDetails::where('config_id', 12)->get();

        $userJson = json_decode(file_get_contents($path), true);
        foreach ($userJson as $key => $data) {

            $empnumber = $data['employment_details']['employee_number'];
            if (preg_match('/^pf-[0-9]+$|^pf[0-9]+$|^PF[0-9]+$|^Pf[0-9]+$|^Pf-[0-9]+$|^[0-9]+$/', $empnumber)) {
                if (strlen($empnumber) == 3) {
                    $empnumber = "PF-$empnumber";
                } elseif (strlen($empnumber) == 5) {

                    $empnumber = str_split($empnumber);
                    $empnumber[0] = strtoupper($empnumber[0]);
                    $empnumber[1] = strtoupper($empnumber[1]);
                    array_splice($empnumber, 2, 0, "-");
                    $empnumber = implode("", $empnumber);
                } elseif (strlen($empnumber) == 6) {

                    $empnumber = str_split($empnumber);
                    $empnumber[0] = strtoupper($empnumber[0]);
                    $empnumber[1] = strtoupper($empnumber[1]);
                    $empnumber = implode("", $empnumber);
                } elseif (strlen($empnumber) == 1) {

                    $empnumber = str_split($empnumber);
                    $empnumber[0] = strtoupper($empnumber[0]);
                    $empnumber[1] = strtoupper($empnumber[1]);
                    array_splice($empnumber, 0, 0, "P", "F", "-", "0", "0");
                    $empnumber = implode("", $empnumber);
                } elseif (strlen($empnumber) == 2) {

                    $empnumber = str_split($empnumber);
                    $empnumber[0] = strtoupper($empnumber[0]);
                    $empnumber[1] = strtoupper($empnumber[1]);
                    array_splice($empnumber, 0, 0, "P", "F", "-", "0");
                    $empnumber = implode("", $empnumber);

                }
            }
            $gender = isset($data['basic_details']['gender']) ? $data['basic_details']['gender'] == 'M' ? 'male' : 'female' : 'male';
            $created_user = User::create([
                'employee_number' => $data['employment_details']['employee_number'],
                'first_name' => $data['basic_details']['firstname'],
                'last_name' => $data['basic_details']['lastname'],
                'contact_number' => $data['basic_details']['contact_one'],
                'contact_number_two' => $data['basic_details']['contact_two'],
                'contact_number_two' => $data['basic_details']['contact_two'],
                'email' => $data['basic_details']['email'],
                'cnic' => $data['basic_details']['cnic'],
                'nationality' => $data['basic_details']['nationality'],
                'gender' => $gender,
                'martial_status' => $data['family_details']['martial_status'] == true ? 'married' : 'unmarried',
                'blood_group' => $data['basic_details']['blood_group'],
                'date_of_birth' => date('Y-m-d', strtotime($userJson[27]['basic_details']['dob']['$date'])),
                'city' => $data['location_details']['city'],
                'country' => $data['location_details']['country'],
                'address' => $data['location_details']['address'],
                'is_active' => 1,
            ]);

            $userId = $created_user->id;
            $userSystemDetails = new UserSystemDetail;
            $hashPass = bcrypt('12345678');

            $userSystemDetails->username = $data['employment_details']['employee_number'];
            $userSystemDetails->password = $hashPass;
            $userSystemDetails->user_id = $userId;

            $userSystemDetails->save();

            foreach ($leave_config as $key => $value) {

                UserLeaves::create([
                    'user_id' => $userId,
                    'quantity' => $value->quantity,
                    'leave_type_id' => $value->leave_type,
                ]);
            }

        }

        return response()->json([
            "data" => $userJson,
            "code" => $this->successStatus,
            "message" => "data saved",
        ], $this->successStatus);
    }

    public function testing(Request $request)
    {

        $file = $request->file('file');
        $data = file_get_contents($file);
        $data = json_decode($data);
        return response()->json([
            "data" => $data,
            "code" => $this->successStatus,
            "message" => "data saved",
        ], $this->successStatus);
    }

}
