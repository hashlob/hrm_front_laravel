<?php

namespace App\Http\Controllers\Api\UserDocument;

use App\Http\Controllers\Controller;
use App\User;
use App\UserDocument;
use Illuminate\Http\Request;

class UserDocumentController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public $uploadPath = "uploads/employees/";

    public function create(Request $request)
    {

        $document_type_id = $request->document_type_id;
        $user_id = $request->user_id;
        $attachment = $request->file('attachment');
        $message = "";
        $user_doc = null;

        $user_details = User::where('id', $user_id)->get()->first();
        $user_doc_check = UserDocument::where([
            'document_type_id' => $document_type_id,
            'user_id' => $user_id,

        ])->get();

        $uploat_to = $this->uploadPath . $user_details->employee_number . "/docs";
        $attachment_name = '';
        if ($attachment != null) {
            $attachment_name = $attachment->getClientOriginalName();
            $attachment->move($uploat_to, $attachment_name);
        }

        if (sizeof($user_doc_check) == 0) {

            $user_doc = UserDocument::create([
                'document_type_id' => $document_type_id,
                'attachment' => $attachment_name,
                'user_id' => $user_id,
            ]);
            $message = "Document Added.";

        } else {
            $user_doc = UserDocument::where([
                'document_type_id' => $document_type_id,
                'user_id' => $user_id,

            ])
                ->update([
                    'document_type_id' => $document_type_id,
                    'attachment' => $attachment_name,
                    'user_id' => $user_id,
                ]);
            $message = "Document Updated.";
        }

        return response()->json([
            "data" => $user_doc,
            "message" => "created.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function getUserDocuments(Request $request)
    {
        $user_id = $request->user_id;
        $userDocs = UserDocument::with('document')->where('user_id', $user_id)
            ->get();
        return response()->json([
            "data" => $userDocs,
            "message" => "created.",
            "code" => $this->successStatus,
        ], $this->successStatus);
    }

    public function getAttachment(Request $request)
    {

        $user_id = $request->user_id;
        $emp_num = USER::where('id', $user_id)->first();

        $file = $this->uploadPath . $emp_num->employee_number . "/docs" . "/" . $request->attachment;

        return response()->download(public_path($file));
    }
}
