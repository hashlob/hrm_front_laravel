<?php

namespace App\Http\Controllers\Api\Allowance;

use App\AllowanceHistory;
use App\Http\Controllers\Controller;
use App\UserAllowance;
use Illuminate\Http\Request;

class AllowanceController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public function addUserAllowance(Request $request)
    {

        $user_id = $request->user_id;
        $allowance_type_id = $request->allowance_type_id;
        $is_monthly = $request->is_monthly;

        $check_allowance = UserAllowance::where([
            'user_id' => $user_id,
            'allowance_type_id' => $allowance_type_id,
        ])
            ->get();

        $user_allowance = null;
        $message = "";
        if (sizeof($check_allowance) == 0) {
            $user_allowance = UserAllowance::create($request->all());
            AllowanceHistory::create($request->all());
            $message = "Allowance Added.";
        } else {
            $user_allowance = UserAllowance::where([
                'user_id' => $user_id,
                'allowance_type_id' => $allowance_type_id,
            ])->
                update([
                'value' => $request->value,
                'remarks' => $request->remarks,
                'value_type' => $request->value_type,
                'is_monthly' => $is_monthly,
            ]);
            $message = "Allowance Updated.";
            AllowanceHistory::create($request->all());

        }

        return response()->json([
            "data" => $user_allowance,
            "code" => $this->successStatus,
            "message" => $message,
        ], $this->successStatus);
    }

    public function getUserAllowances(Request $request)
    {

        $typeFilter = $request->typeFilter;
        $user_allowance = UserAllowance::with('allowanceType', 'user');

        if ($typeFilter) {
            $user_allowance = $user_allowance->where('allowance_type_id', $typeFilter);
        }

        $user_allowance = $user_allowance->get();

        return response()->json([
            "data" => $user_allowance,
            "code" => $this->successStatus,
            "message" => "Allowances",
        ], $this->successStatus);
    }

    public function delete(Request $request)
    {

        $id = $request->id;
        $user_allowance = UserAllowance::destroy($id);

        return response()->json([
            "data" => $user_allowance,
            "code" => $this->successStatus,
            "message" => "Allowance Deleted.",
        ], $this->successStatus);
    }
}
