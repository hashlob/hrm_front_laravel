<?php

namespace App\Http\Controllers\Api\ExperienceDetail;

use App\ExperienceDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExperienceDetailController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public $uploadPath = "uploads/employees/";

    public function create(Request $request)
    {

        $rules = [
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $userId = $request->user_id;
        $id = $request->id;
        $experienceDetails = ExperienceDetail::where(['id' => $id, 'user_id' => $userId])->get();

        $salary_slip = $request->file('salary_slip');
        $experience_letter = $request->file('experience_letter');
        $salary_slip_name = null;
        $experience_letter_name = null;
        $uploat_to = $this->uploadPath . $request->employee_number . "/docs";

        if ($salary_slip != null) {
            $salary_slip_name = $salary_slip->getClientOriginalName();
            $salary_slip->move($uploat_to, $salary_slip_name);
        }
        if ($experience_letter != null) {
            $experience_letter_name = $experience_letter->getClientOriginalName();
            $experience_letter->move($uploat_to, $experience_letter_name);
        }

        if (count($experienceDetails) == 0) {
            $ExperienceDetail = new ExperienceDetail;
            $ExperienceDetail->employer = $request->employer;
            $ExperienceDetail->position = $request->position;
            $ExperienceDetail->joining_date = $request->joining_date;
            $ExperienceDetail->leaving_date = $request->leaving_date;
            $ExperienceDetail->user_id = $request->user_id;
            $ExperienceDetail->reason_for_leaving = $request->reason_for_leaving;
            $ExperienceDetail->last_salary = $request->last_salary;
            $ExperienceDetail->salary_slip = $salary_slip_name;
            $ExperienceDetail->experience_letter = $experience_letter_name;

            $ExperienceDetail->save();

            return response()->json([
                "data" => $ExperienceDetail,
                "code" => $this->successStatus,
                "message" => "Experience Details Created.",
            ], $this->successStatus);
        } else {

            $ExperienceDetail = ExperienceDetail::where(['id' => $id, 'user_id' => $userId])->update([
                'employer' => $request->employer,
                'position' => $request->position,
                'joining_date' => $request->joining_date,
                'leaving_date' => $request->leaving_date,
                'last_salary' => $request->last_salary,
                'reason_for_leaving' => $request->reason_for_leaving,
                'salary_slip' => $salary_slip_name,
                'experience_letter' => $experience_letter_name,
            ]);

            return response()->json([
                "data" => $ExperienceDetail,
                "code" => $this->successStatus,
                "message" => "Experience Details Updated.",
            ], $this->successStatus);
        }

    }

    public function getUsersExperienceDetail(Request $request)
    {
        $userId = $request->userId;
        $experienceDetails = ExperienceDetail::where('user_id', $userId)->get();

        return response()->json([
            "data" => $experienceDetails,
            "code" => $this->successStatus,
            "message" => "Experience Details.",
        ], $this->successStatus);

    }

    public function fetchExperienceDetailById(Request $request)
    {
        $id = $request->id;
        $experienceDetails = ExperienceDetail::find($id)->first();

        return response()->json([
            "data" => $experienceDetails,
            "code" => $this->successStatus,
            "message" => "Experience Details.",
        ], $this->successStatus);
    }
}
