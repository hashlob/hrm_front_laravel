<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\LeaveConfigurationDetails;
use App\User;
use App\UserLeaves;
use App\UserSystemDetail;
use File;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error.";

    public $uploadPath = "uploads/employees/";

    public function create(Request $request)
    {
        // dd($request->all());

        $profile_image = $request->file('profile_image');
        $address_verification = $request->file('address_verification');
        $cnic_attachment = $request->file('cnic_attachment');
        $profile_image_name = null;
        $address_verification_name = null;
        $cnic_attachment_name = null;
        $uploat_to = $this->uploadPath . $request->employee_number . "/docs";
        $userId = $request->user_id;

        if ($userId) {
            if ($profile_image != null) {
                $profile_image_name = $profile_image->getClientOriginalName();
                $profile_image->move($uploat_to, $profile_image_name);
            }
            if ($address_verification != null) {
                $address_verification_name = $address_verification->getClientOriginalName();
                $address_verification->move($uploat_to, $address_verification_name);
            }
            if ($cnic_attachment != null) {
                $cnic_attachment_name = $cnic_attachment->getClientOriginalName();
                $cnic_attachment->move($uploat_to, $cnic_attachment_name);
            }
            $user = USER::where('id', $userId)
                ->update([
                    "first_name" => $request->first_name,
                    "employee_number" => $request->employee_number,
                    "last_name" => $request->last_name,
                    "contact_number" => $request->contact_number,
                    "email" => $request->email,
                    "cnic" => $request->cnic,
                    "nationality" => $request->nationality,
                    "gender" => $request->gender,
                    "martial_status" => $request->martial_status,
                    "profile_image" => $profile_image_name,
                    "address_verification" => $address_verification_name,
                    "cnic_attachment" => $cnic_attachment_name,
                ]);

            $userSystemDetails = UserSystemDetail::where('id', $userId)
                ->update([
                    "username" => $request->username,
                    "role_id" => $request->role_id,
                ]);

            $message = "User Updated.";

        } else {

            File::makeDirectory(public_path() . '/uploads/employees/' . $request->employee_number, $mode = 0777, true, true);

            if ($profile_image != null) {
                $profile_image_name = $profile_image->getClientOriginalName();
                $profile_image->move($uploat_to, $profile_image_name);
            }
            if ($address_verification != null) {
                $address_verification_name = $address_verification->getClientOriginalName();
                $address_verification->move($uploat_to, $address_verification_name);
            }
            if ($cnic_attachment != null) {
                $cnic_attachment_name = $cnic_attachment->getClientOriginalName();
                $cnic_attachment->move($uploat_to, $cnic_attachment_name);
            }

            $leave_config_id = $request->leave_config_id;
            $leave_config = null;
            if ($leave_config_id == null) {
                $leave_config = LeaveConfigurationDetails::where('config_id', 12)->get();
            } else {

                $leave_config = LeaveConfigurationDetails::where('config_id', $leave_config_id)->get();
            }

            $hashPass = bcrypt($request->password);
            $user = new User;

            $user->employee_number = $request->employee_number;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->contact_number = $request->contact_number;
            $user->email = $request->email;
            $user->cnic = $request->cnic;
            $user->nationality = $request->nationality;
            $user->gender = $request->gender;
            $user->martial_status = $request->martial_status;
            $user->profile_image = $profile_image_name;
            $user->address_verification = $address_verification_name;
            $user->cnic_attachment = $cnic_attachment_name;
            $user->is_active = 1;

            $user->save();

            $userSystemDetails = new UserSystemDetail;

            $userSystemDetails->username = $request->username;
            $userSystemDetails->password = $hashPass;
            $userSystemDetails->role_id = $request->role_id;
            $userSystemDetails->user_id = $user->id;

            $userSystemDetails->save();
            $userId = $user->id;

            foreach ($leave_config as $key => $value) {

                UserLeaves::create([
                    'user_id' => $userId,
                    'quantity' => $value->quantity,
                    'leave_type_id' => $value->leave_type,
                ]);
            }

            $message = "User Created.";
        }

        return response()->json([
            "data" => $user,
            "message" => $message,
            "code" => $this->successStatus,
            'user_id' => $userId,
        ], $this->successStatus);
    }

    public function getAll(Request $request)
    {
        $search = $request->all();
        $user = User::with('systemDetail', 'employmentDetails');

        $user = $this->userSearch($search, $user);

        $user = $user->get();

        return response()->json([
            "data" => $user,
            "code" => $this->successStatus,
            "message" => "Users..",
        ]);
    }

    public function userSearch($search, $user)
    {
        if (isset($search['first_name'])) {
            $user = $user->where('first_name', 'like', "%" . $search['first_name'] . "%");
        }

        if (isset($search['last_name'])) {
            $user = $user->where('last_name', 'like', "%" . $search['last_name'] . "%");
        }

        if (isset($search['email'])) {
            $user = $user->where('email', 'like', "%" . $search['email'] . "%");
        }

        if (isset($search['employee_number'])) {
            $user = $user->where('employee_number', 'like', "%" . $search['employee_number'] . "%");
        }

        if (isset($search['cnic'])) {
            $user = $user->where('cnic', 'like', "%" . $search['cnic'] . "%");
        }

        if (isset($search['city'])) {
            $user = $user->where('city', 'like', "%" . $search['city'] . "%");
        }

        if (isset($search['gender'])) {
            $user = $user->where('gender', $search['gender']);
        }

        if (isset($search['is_active'])) {
            $user = $user->where('is_active', $search['is_active']);
        }

        if (isset($search['employee_type_id'])) {
            $user = $user->whereHas('employmentDetails', function ($query) use ($search) {
                $query->where('employee_type_id', $search['employee_type_id']);
            });
        }
        
        if (isset($search['department_id'])) {
            $user = $user->whereHas('employmentDetails', function ($query) use ($search) {
                $query->where('department_id', $search['department_id']);
            });
        }

        if (isset($search['unit_id'])) {
            $user = $user->whereHas('employmentDetails', function ($query) use ($search) {
                $query->where('unit_id', $search['unit_id']);
            });
        }

        if (isset($search['designation_id'])) {
            $user = $user->whereHas('employmentDetails', function ($query) use ($search) {
                $query->where('designation_id', $search['designation_id']);
            });
        }

        return $user;
    }

    public function getUserDetail(Request $request)
    {
        $userId = $request->userId;

        $user = User::where('id', $userId)->with('systemDetail')->get();
        return response()->json([
            "data" => $user,
            "code" => $this->successStatus,
            "message" => "Users.",
        ]);
    }

    public function changeUserStatus(Request $request)
    {
        $userId = $request->userId;
        $status = $request->status;

        $user = User::where('id', $userId)->update([
            'is_active' => $status,
        ]);
        $message = "Status changed to ";
        $status = $status == 0 ? "Inactive" : "Active";

        return response()->json([
            "data" => $user,
            "code" => $this->successStatus,
            "message" => $message . $status,
            'user_id' => $userId,
        ]);
    }

    public function getAllSalaries(Request $request)
    {
        $employmentDetails = User::where('id', '<>', 1)
            ->with('employmentDetails.department', 'employmentDetails.unit')->get();

        return response()->json([
            "data" => $employmentDetails,
            "code" => $this->successStatus,
            "message" => "Salary Details.",
        ], $this->successStatus);

    }

}
