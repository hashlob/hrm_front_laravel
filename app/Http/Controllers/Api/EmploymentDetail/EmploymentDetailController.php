<?php

namespace App\Http\Controllers\Api\EmploymentDetail;

use App\EmploymentDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmploymentDetailController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 500;
    public $unauthorisedStatus = 401;
    public $validationStatus = 422;

    public $errorMessage = "Api Failed";
    public $unauthorisedMessage = "Unauthorised";
    public $validationMessage = "Validation Error";

    public function create(Request $request)
    {
        date_default_timezone_set("Asia/Karachi");

        // dd($request->all());

        $rules = [
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors(),
                'code' => $this->validationStatus,
                'message' => $this->validationMessage,
            ], $this->validationStatus);
        }
        $userId = $request->user_id;
        $employmentDetails = EmploymentDetail::where('user_id', $userId)->get();

        if (count($employmentDetails) == 0) {
            $employmentDetail = EmploymentDetail::create($request->all());

            return response()->json([
                "data" => $employmentDetail,
                "code" => $this->successStatus,
                "message" => "Employment Details Created.",
            ], $this->successStatus);
        } else {

            $employmentDetail = EmploymentDetail::where('user_id', $userId)->update([
                'employee_type_id' => $request->employee_type,
                'confirmation_date' => $request->confirmation_date,
                'quit_date' => $request->quit_date,
                'salary' => $request->salary,
                'grade' => $request->grade,
                'payment_type' => $request->payment_type,
                'designation_id' => $request->designation_id,
                'unit_id' => $request->unit_id,
                'is_tranfer_allow' => $request->is_tranfer_allow,
                'transfer_period' => $request->transfer_period,
                'department_id' => $request->department_id,
                'shift_timein' => $request->shift_timein,
                'shift_timeout' => $request->shift_timeout,
                'next_transfer' => $request->next_transfer,
            ]);

            return response()->json([
                "data" => $employmentDetail,
                "code" => $this->successStatus,
                "message" => "Employment Details Updated.",
            ], $this->successStatus);
        }

    }

    public function getUsersEmploymentDetail(Request $request)
    {
        $userId = $request->userId;
        $employmentDetails = EmploymentDetail::with('employeeType')->where('user_id', $userId)->get();

        return response()->json([
            "data" => $employmentDetails,
            "code" => $this->successStatus,
            "message" => "Employment Details.",
        ], $this->successStatus);

    }

    public function getProbationList(Request $request)
    {
        $probationList = EmploymentDetail::with("user.systemDetail")->where('employee_type_id', 22)->get();

        return response()->json([
            "data" => $probationList,
            "code" => $this->successStatus,
            "message" => "Probation List.",
        ], $this->successStatus);
    }

}
