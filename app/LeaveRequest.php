<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{

    protected $table = "leave_request";
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function leaveType()
    {
        return $this->belongsTo('App\DbVariableDetail','leave_type');
    }

    public function appliedBy()
    {
        return $this->belongsTo('App\User','applied_by');
    }

    public function nextInline()
    {
        return $this->belongsTo('App\User','next_inline_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
        
    }
    
}
