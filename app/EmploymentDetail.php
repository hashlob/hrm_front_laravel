<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmploymentDetail extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function designation()
    {
        return $this->belongsTo('App\Designation');
    }

    public function employeeType()
    {
        return $this->belongsTo('App\DbVariableDetail','employee_type_id');
    }

}
