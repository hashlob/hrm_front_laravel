<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLine extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $table = "request_line";

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }

    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id');
    }

}
