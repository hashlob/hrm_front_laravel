<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }
}
