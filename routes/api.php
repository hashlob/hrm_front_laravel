<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('user', function (Request $request) {
//     return $request->user();
// });

// header("Access-Control-Allow-Origin: *");
// header('Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS');
// header('Access-Control-Allow-Headers: Origin, Content-Type,X-Auth-Token, Authorization, X-Request-With');
// header('Access-Control-Allow-Credentials: true');

Route::post('auth', function (Request $request) {
    return "Hello";
});

// Route::get('/role', 'Api\Role\RoleController@getAll');

// --------------------------- Auth routes

Route::post('auth/login', 'Api\Auth\ApiAuthController@login')->name("login");
// Route::post('auth/create', 'Api\Auth\ApiAuthController@create');
Route::get('/readDepartmentFile', 'Api\test\TestController@readDepartmentFile');
Route::get('/readDesignationsFile', 'Api\test\TestController@readDesignationsFile');
Route::get('/readEmployeesFile', 'Api\test\TestController@readEmployeesFile');
Route::post('/testing', 'Api\test\TestController@testing');
Route::post('/markAttendanceFromMachine', 'Api\Attendance\AttendanceController@markAttendanceFromMachine');
Route::post('/addDevice', 'Api\Machine\MachineController@create');
Route::get('/getAllDevices', 'Api\Machine\MachineController@getMachines');

// Auth::routes();

Route::group(['middleware' => 'auth:api'], function () {

    //---------------------------- Roles Routes
    Route::group(["prefix" => "role"], function () {

        Route::get('/getRoleById/{id}', 'Api\Role\RoleController@getRoleById');
        Route::post('/', 'Api\Role\RoleController@create');
        Route::get('/', 'Api\Role\RoleController@getAll');
        Route::post('/delete', 'Api\Role\RoleController@delete');

    });

    //---------------------------- Permissions Routes
    Route::group(["prefix" => "permission"], function () {

        Route::post('/', 'Api\Permissions\PermissionController@create');
        Route::get('/getPermissionById/{id}', 'Api\Permissions\PermissionController@getPermissionById');
        Route::get('/', 'Api\Permissions\PermissionController@getAll');
        Route::get('/rolePermission/getPermissionByRole', 'Api\Permissions\PermissionController@getPermissionByRole');
        Route::post('/rolePermission', 'Api\Permissions\PermissionController@addRolePermission');
        Route::post('/rolePermission/delete', 'Api\Permissions\PermissionController@delete');

    });

    //---------------------------- designation Routes
    Route::group(["prefix" => "designation"], function () {
        Route::post('/', 'Api\Designation\DesignationController@create');
        Route::get('/', 'Api\Designation\DesignationController@getAll');
        Route::post('/delete', 'Api\Designation\DesignationController@delete');
    });

    //---------------------------- Units Routes
    Route::group(["prefix" => "unit"], function () {
        Route::post('/', 'Api\Unit\UnitController@create');
        Route::get('/', 'Api\Unit\UnitController@getAll');
        Route::post('/delete', 'Api\Unit\UnitController@delete');
        Route::get('/getUnitById/{id}', 'Api\Unit\UnitController@geUnitById');
    });

    //---------------------------- Departments Routes
    Route::group(["prefix" => "department"], function () {
        Route::post('/', 'Api\Department\DepartmentController@create');
        Route::get('/', 'Api\Department\DepartmentController@getAll');
        Route::get('/getDepartmentById/{id}', 'Api\Department\DepartmentController@getDepartmentById');
        Route::post('/delete', 'Api\Department\DepartmentController@delete');

        // ---------------------- Request line Routes
        Route::post('/requestLine', 'Api\Department\RequestLineController@create');
        Route::get('/requestLine/getForDepartment', 'Api\Department\RequestLineController@getForDepartment');
        Route::get('/requestLine/delete/{id}', 'Api\Department\RequestLineController@delete');

    });

    //---------------------------- user details Routes
    Route::group(["prefix" => "user"], function () {
        Route::get('/getProbationList', 'Api\EmploymentDetail\EmploymentDetailController@getProbationList');
        Route::post('/', 'Api\User\UserController@create');
        Route::get('/', 'Api\User\UserController@getAll');
        Route::get('/{userId}', 'Api\User\UserController@getUserDetail');
        Route::post('/changeUserStatus', 'Api\User\UserController@changeUserStatus');

    });

    //---------------------------- Hiring details Routes
    Route::group(["prefix" => "hiring"], function () {
        Route::post('/', 'Api\HiringDetail\HiringDetailController@create');
        Route::get('/{userId}', 'Api\HiringDetail\HiringDetailController@getUsersHiringDetail');
    });

    //---------------------------- Employment details Routes
    Route::group(["prefix" => "employment"], function () {
        Route::post('/', 'Api\EmploymentDetail\EmploymentDetailController@create');
        Route::get('/{userId}', 'Api\EmploymentDetail\EmploymentDetailController@getUsersEmploymentDetail');
    });

    //---------------------------- Education details Routes
    Route::group(["prefix" => "education"], function () {
        Route::get('/getMarksheet', 'Api\EducationDetail\EducationDetailController@getMarksheet');
        Route::get('/getDegree', 'Api\EducationDetail\EducationDetailController@getDegree');
        Route::post('/', 'Api\EducationDetail\EducationDetailController@create');
        Route::get('/{userId}', 'Api\EducationDetail\EducationDetailController@getUsersEducationDetail');
        Route::get('/fetchEducationDetailById/{id}', 'Api\EducationDetail\EducationDetailController@fetchEducationDetailById');
        Route::post('/delete', 'Api\EducationDetail\EducationDetailController@delete');

    });

    //---------------------------- Experience details Routes
    Route::group(["prefix" => "experience"], function () {
        Route::post('/', 'Api\ExperienceDetail\ExperienceDetailController@create');
        Route::get('/{userId}', 'Api\ExperienceDetail\ExperienceDetailController@getUsersExperienceDetail');
        Route::get('/fetchExperienceDetailById/{id}', 'Api\ExperienceDetail\ExperienceDetailController@fetchExperienceDetailById');

    });

    //---------------------------- Family details Routes
    Route::group(["prefix" => "family"], function () {
        Route::post('/', 'Api\FamilyDetail\FamilyDetailController@create');
        Route::get('/{userId}', 'Api\FamilyDetail\FamilyDetailController@getUsersFamilyDetail');
        Route::get('/getFamilyDetailById/{id}', 'Api\FamilyDetail\FamilyDetailController@getFamilyDetailById');
    });

    //---------------------------- Family details Routes
    Route::group(["prefix" => "document"], function () {
        Route::get('/getAttachment', 'Api\UserDocument\UserDocumentController@getAttachment');
        Route::post('/', 'Api\UserDocument\UserDocumentController@create');
        Route::get('/{user_id}', 'Api\UserDocument\UserDocumentController@getUserDocuments');

    });

    //---------------------------- Emergency Contacts details Routes
    Route::group(["prefix" => "emergencyContacts"], function () {
        Route::post('/', 'Api\EmergencyContacts\EmergencyContactsDetailController@create');
        Route::get('/{userId}', 'Api\EmergencyContacts\EmergencyContactsDetailController@getUsersEmergencyetail');
        Route::get('/fetchEmergencyDetailById/{id}', 'Api\EmergencyContacts\EmergencyContactsDetailController@fetchEmergencyDetailById');

    });

    //---------------------------- Shared Routes
    Route::group(["prefix" => "shared"], function () {
        Route::post('/createSytemVarable', 'Api\Shared\SharedController@createSytemVarable');
        Route::get('/getUsersForSelect', 'Api\Shared\SharedController@getUsersForSelect');
        Route::get('/getRolesForSelect', 'Api\Shared\SharedController@getRolesForSelect');
        Route::get('/getUnitForSelect', 'Api\Shared\SharedController@getUnitForSelect');
        Route::get('/getDepartmentForSelect', 'Api\Shared\SharedController@getDepartmentForSelect');
        Route::get('/getDesignationForSelect', 'Api\Shared\SharedController@getDesignationForSelect');
        Route::get('/getDBVariable', 'Api\Shared\SharedController@getDBVariable');
        Route::get('/getSystemVariable', 'Api\Shared\SharedController@getSystemVariable');
        Route::get('/fetchOptionById', 'Api\Shared\SharedController@fetchOptionById');
        Route::get('/getNotifications', 'Api\Shared\SharedController@getNotifications');
        Route::get('/readNotifications/{id}', 'Api\Shared\SharedController@readNotifications');

    });

    //---------------------------- Routes for leave_configuration.
    Route::group(['prefix' => 'leaveConfiguration'], function () {
        // Route::post('/', 'Api\LeaveConfig\LeaveConfigurationController@create');
        // Route::get('/delete/{id}', 'Api\LeaveConfig\LeaveConfigurationController@delete');
        // Route::get('/getAll', 'Api\LeaveConfig\LeaveConfigurationController@getAll');

        Route::post('/', 'Api\LeaveConfig\LeaveConfigurationDetailsController@create');
        Route::get('/delete/{id}', 'Api\LeaveConfig\LeaveConfigurationDetailsController@delete');
        Route::get('/getForConfig', 'Api\LeaveConfig\LeaveConfigurationDetailsController@getForConfig');

    });

    //----------------------------- Routes for leave_request.
    Route::group(array('prefix' => 'leaveRequest'), function () {
        Route::post('/apply', 'Api\Leave\LeaveRequestContoller@applyForLeave');
        Route::post('/processLeave', 'Api\Leave\LeaveRequestContoller@processLeaveApproval');
        Route::get('/getLeavesDetails', 'Api\Leave\LeaveRequestContoller@getLeavesDetails');

    });

    //----------------------------- Routes for attendance.
    Route::group(array('prefix' => 'attendance'), function () {
        Route::post('/markAttendance', 'Api\Attendance\AttendanceController@markAttendance');
        Route::get('/getAttendanceReport', 'Api\Attendance\AttendanceController@getAttendanceReport');

    });

    //----------------------------- Routes for User Allownaces.
    Route::group(array('prefix' => 'allowance'), function () {
        Route::post('/addUserAllowance', 'Api\Allowance\AllowanceController@addUserAllowance');
        Route::get('/getUserAllowances', 'Api\Allowance\AllowanceController@getUserAllowances');
        Route::get('/delete/{id}', 'Api\Allowance\AllowanceController@delete');

    });

    //------------------------------ routes for zone.
    Route::group(array('prefix' => 'zone'), function () {
        Route::post('/', 'Api\Zone\ZoneController@create');
        Route::get('/fetchZoneById/{id}', 'Api\Zone\ZoneController@fetchZoneById');
        Route::get('/', 'Api\Zone\ZoneController@getAll');
        Route::post('/changeZoneStatus', 'Api\Zone\ZoneController@changeZoneStatus');
    });

    //------------------------------- routes for transfer_requests.
    Route::group(array('prefix' => 'transferRequests'), function () {
        Route::post('/generateRequest', 'Api\TransferRequests\TransferRequestsController@generateRequest');
        Route::get('/getTransferRequests', 'Api\TransferRequests\TransferRequestsController@getTransferRequests');
        Route::post('/processTransferRequest', 'Api\TransferRequests\TransferRequestsController@processTransferRequest');
        Route::get('/checkTransfers', 'Api\TransferRequests\TransferRequestsController@checkTransfers');

    });

    //------------------------------- routes for Hiring requests.
    Route::group(array('prefix' => 'hiringRequests'), function () {
        Route::post('/', 'Api\HiringRequest\HiringRequestContoller@create');
        Route::post('/processHiringRequest', 'Api\HiringRequest\HiringRequestContoller@processHiringRequest');
        Route::get('/', 'Api\HiringRequest\HiringRequestContoller@getHiringRequests');
        Route::get('/fetchHiringById/{id}', 'Api\HiringRequest\HiringRequestContoller@fetchHiringById');

    });

    //------------------------------- routes for job Post.
    Route::group(array('prefix' => 'jobPost'), function () {
        Route::post('/', 'Api\JobPost\JobPostContoller@create');
        Route::post('/changeStatus', 'Api\JobPost\JobPostContoller@changeStatus');
        Route::get('/', 'Api\JobPost\JobPostContoller@getJobPosts');
        Route::get('/getJobPostById/{id}', 'Api\JobPost\JobPostContoller@getJobPostById');

        Route::post('/uploadCv', 'Api\JobPost\CvBankController@upload');
        Route::post('/cv/changeStatus', 'Api\JobPost\CvBankController@changeStatus');
        Route::get('/getAllCvs', 'Api\JobPost\CvBankController@getAllCvs');
        Route::get('/downloadCv', 'Api\JobPost\CvBankController@downloadCv');

        Route::post('/scheduleInterview', 'Api\JobPost\InterviewController@create');
        Route::get('/getAllInterviews', 'Api\JobPost\InterviewController@getAllInterviews');
        Route::get('/fetchInterviewById/{id}', 'Api\JobPost\InterviewController@fetchInterviewById');
        Route::post('/interview/changeStatus', 'Api\JobPost\InterviewController@changeStatus');

    });

    //------------------------------- routes for Payroll.
    Route::group(array('prefix' => 'payroll'), function () {
        Route::post('/addBonus', 'Api\Payroll\BonusController@addBonus');
        Route::get('/getBonuses', 'Api\Payroll\BonusController@getBonuses');
        Route::get('/getBonusById/{id}', 'Api\Payroll\BonusController@getBonusById');
        Route::post('/removeBonus/{id}', 'Api\Payroll\BonusController@removeBonus');

        Route::post('/addDeduction', 'Api\Payroll\DeductionController@addDeduction');
        Route::get('/getDeductions', 'Api\Payroll\DeductionController@getDeductions');
        Route::get('/getDeductionById/{id}', 'Api\Payroll\DeductionController@getDeductionById');
        Route::post('/removeDeduction/{id}', 'Api\Payroll\DeductionController@removeDeduction');

        Route::post('/addFine', 'Api\Payroll\FineController@addFine');
        Route::get('/getFines', 'Api\Payroll\FineController@getFines');
        Route::get('/getFineById/{id}', 'Api\Payroll\FineController@getFineById');
        Route::post('/removeFine/{id}', 'Api\Payroll\FineController@removeFine');

        Route::post('/addLoan', 'Api\Payroll\LoanController@addLoan');
        Route::get('/getLoans', 'Api\Payroll\LoanController@getLoans');
        Route::get('/getLoanById/{id}', 'Api\Payroll\LoanController@getLoanById');

        Route::get('/getAllSalaries', 'Api\User\UserController@getAllSalaries');
        Route::get('/getEmployeePayslip', 'Api\Payroll\PayrollController@getEmployeePayslip');
        Route::get('/generatePayroll', 'Api\Payroll\PayrollController@generatePayroll');
        Route::post('/markSalaryIncreament', 'Api\Payroll\PayrollController@markSalaryIncreament');
        Route::post('/savePayroll', 'Api\Payroll\PayrollController@savePayroll');
        Route::post('/lockPayroll', 'Api\Payroll\PayrollController@lockPayroll');

    });

    //----------------------------- Routes for Dashboard.
    Route::group(array('prefix' => 'dashboard'), function () {
        Route::get('/getDisplayCardsData', 'Api\Dashboard\DashboardController@getDisplayCardsData');
        Route::get('/getAttendaceCardsData', 'Api\Dashboard\DashboardController@getAttendaceCardsData');

    });

});

Route::any('{url?}/{sub_url?}/{sub_sub_url?}/{sub_sub_sub_url?}/{sub_sub_sub_sub_url?}', function () {
    return response()->json([
        'data' => false,
        'message' => 'Route not Found.',
        'code' => 404,
    ], 404);
});
