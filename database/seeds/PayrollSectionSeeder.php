<?php

use App\DbVariable;
use Illuminate\Database\Seeder;

class PayrollSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payrollSectionArray = [
            'Bonus Type',
            'Deduction Type',
            'Fine Type',
            'Loan Type',
        ];

        foreach ($payrollSectionArray as $key => $value) {
            DbVariable::create([
                'type' => $value,
            ]);
        }
    }
}
