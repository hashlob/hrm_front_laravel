<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Super Admin',
            'HR Manager',
            'Manager',
            'Employee',
        ];

        foreach ($roles as $key => $value) {
            Role::create([
                'name' => $value,
            ]);
        }
    }
}
