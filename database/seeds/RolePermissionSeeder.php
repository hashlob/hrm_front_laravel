<?php

use App\Permission;
use App\RolePermission;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $allPermissions = Permission::all();

        foreach ($allPermissions as $key => $value) {
            RolePermission::create([
                'role_id' => 1,
                'permission_id' => $value->id,
            ]);
        }

        foreach ($allPermissions as $key => $value) {
            RolePermission::create([
                'role_id' => 2,
                'permission_id' => $value->id,
            ]);
        }

        $employeePermission = [
            1,
            6,
            7,
            8,
            10,
            12,
            32,
            38
        ];

        foreach ($employeePermission as $key => $value) {
            RolePermission::create([
                'role_id' => 4,
                'permission_id' => $value,
            ]);
        }

    }
}
