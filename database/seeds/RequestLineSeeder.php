<?php

use Illuminate\Database\Seeder;
use App\DbVariable;
use App\DbVariableDetail;

class RequestLineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Request Lines";

        $type->save();

        $options = [
            'Leave',
            'Transfer Request',
            'CV Approval'
        ];

        foreach ($options as $key => $option) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $option
            ]);
        }
    }
}
