<?php

use Illuminate\Database\Seeder;
use App\DbVariable;
use App\DbVariableDetail;

class EmployeeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Employee Type";

        $type->save();

        $docs = [
            'Permanent',
            'Intern',
            'Probation',
          
        ];

        foreach ($docs as $key => $leave) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $leave
            ]);
        }
    }
}
