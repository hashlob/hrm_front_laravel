<?php

use Illuminate\Database\Seeder;
use App\DbVariable;
use App\DbVariableDetail;

class UnitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Unit";

        $type->save();

        $payments = [
            'Head Office',
            'Branch',
        ];

        foreach ($payments as $key => $payment) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $payment,
            ]);
        }
    }
}
