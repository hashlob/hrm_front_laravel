<?php

use App\User;
use App\UserSystemDetail;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new User;

        $user->employee_number = "000";
        $user->first_name = "Super";
        $user->last_name = "Admin";
        $user->contact_number = "000";
        $user->email = "admin@admin.com";
        $user->cnic = "112312321";
        $user->gender = "male";
        $user->martial_status = "married";
        $user->is_active = 1;

        $user->save();

        $userSystemDetails = new UserSystemDetail;

        $userSystemDetails->username = "000";
        $userSystemDetails->password = Hash::make('admin');
        $userSystemDetails->role_id = 1;
        $userSystemDetails->user_id = $user->id;

        $userSystemDetails->save();

        $user2 = new User;

        $user2->employee_number = "001";
        $user2->first_name = "HR";
        $user2->last_name = "Manager";
        $user2->contact_number = "0000";
        $user2->email = "HR@admin.com";
        $user2->cnic = "1123123211";
        $user2->gender = "male";
        $user2->martial_status = "married";
        $user2->is_active = 1;

        $user2->save();

        $userSystemDetails2 = new UserSystemDetail;

        $userSystemDetails2->username = "001";
        $userSystemDetails2->password = Hash::make('admin');
        $userSystemDetails2->role_id = 2;
        $userSystemDetails2->user_id = $user2->id;

        $userSystemDetails2->save();

    }
}
