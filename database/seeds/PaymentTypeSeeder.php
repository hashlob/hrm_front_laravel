<?php

use Illuminate\Database\Seeder;
use App\DbVariable;
use App\DbVariableDetail;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Payment";

        $type->save();

        $payments = [
            'cash',
            'pay order',
            'transfer',
            'cheque'
        ];

        foreach ($payments as $key => $payment) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $payment
            ]);
        }
    }
}
