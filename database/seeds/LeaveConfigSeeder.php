<?php

use App\DbVariable;
use App\DbVariableDetail;
use App\LeaveConfigurationDetails;
use Illuminate\Database\Seeder;

class LeaveConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Leave Config";

        $type->save();

        $dbVariableDetail = new DbVariableDetail;
        $dbVariableDetail->type_id = $type->id;
        $dbVariableDetail->options = 'General';

        $dbVariableDetail->save();

        $leavesConfigs = DbVariableDetail::where('type_id', 1)->get();

        foreach ($leavesConfigs as $key => $leave) {
            LeaveConfigurationDetails::create([
                'config_id' => $dbVariableDetail->id,
                'leave_type' => $leave->id,
                'quantity' => 10,
            ]);
        }
    }
}
