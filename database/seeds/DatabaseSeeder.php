<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(LeaveTypeSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(UnitTypeSeeder::class);
        $this->call(RequestLineSeeder::class);
        $this->call(LeaveConfigSeeder::class);
        $this->call(AllowanceSeeder::class);
        $this->call(PayrollSectionSeeder::class);
        $this->call(DocumentTypeSeeder::class);
        $this->call(EmployeeTypeSeeder::class);
        
        
    }
}
