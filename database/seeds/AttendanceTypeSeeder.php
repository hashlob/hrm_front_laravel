<?php

use App\DbVariable;
use App\DbVariableDetail;
use Illuminate\Database\Seeder;

class AttendanceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Attendance Type";

        $type->save();

        $options = [
            'Present',
            'Absent',
            'Leave',
            'Holiday'
        ];

        foreach ($options as $key => $option) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $option,
            ]);
        }
    }
}
