<?php

use Illuminate\Database\Seeder;
use App\DbVariable;
use App\DbVariableDetail;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Document Type";

        $type->save();

        $docs = [
            'Address Verfication',
          
        ];

        foreach ($docs as $key => $leave) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $leave
            ]);
        }
    }
}
