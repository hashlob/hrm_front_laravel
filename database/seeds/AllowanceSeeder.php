<?php

use App\DbVariable;
use App\DbVariableDetail;
use Illuminate\Database\Seeder;

class AllowanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Allowance";

        $type->save();

        $options = [
            'Provident Funds',
        ];

        foreach ($options as $key => $option) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $option,
            ]);
        }
    }
}
