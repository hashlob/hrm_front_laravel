<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'Dashboard',
            'Users',
            'Users List',
            'User Edit',
            'User Add',
            'Leaves Management',
            'Leaves Requests',
            'Apply for Leave',
            'Leave Config',
            'Attendences',
            'Mark Attendence',
            'Attendance Report',
            'Transfer Management',
            'Request Transfer',
            'Requests List',
            'Allowances',
            'User Allowances',
            'System Settings',
            'Manage Roles',
            'Manage Departments',
            'Manage Units',
            'Manage Zone',
            'Manage Designations',
            'System Variable',
            'Request Line',
            'Permissions',
            'Hiring Management',
            'Job Posts',
            'Cv bank',
            'Interviews',
            'Hiring Request',
            'Payroll Management',
            'Bonuses',
            'Fines',
            'Deductions',
            'Loan',
            'Employee Salaries',
            'Payslip',
            'Payroll Sheet',
            'Probation List'
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission,
            ]);
        }
    }
}
