<?php

use Illuminate\Database\Seeder;
use App\DbVariable;
use App\DbVariableDetail;

class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new DbVariable;

        $type->type = "Leaves";

        $type->save();

        $leaves = [
            'Sick',
            'Casual',
            'Annual'
        ];

        foreach ($leaves as $key => $leave) {
            DbVariableDetail::create([
                'type_id' => $type->id,
                'options' => $leave
            ]);
        }
    }
}
