<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_number');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('contact_number');
            $table->string('contact_number_two');
            $table->string('email');
            $table->string('cnic');
            $table->string('nationality');
            $table->enum('gender',['male','female']);
            $table->enum('martial_status',['married','unmarried']);
            $table->string('blood_group');
            $table->string('profile_image');
            $table->string('address_verification');
            $table->string('cnic_attachment');
            $table->date('date_of_birth');
            $table->string('city');
            $table->string('country');
            $table->text('address');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
