<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->string('employee_number')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('employee_salary')->nullable();
            $table->string('employee_bonuses')->nullable();
            $table->string('employee_fines')->nullable();
            $table->string('employee_deductions')->nullable();
            $table->string('employee_allowances')->nullable();
            $table->string('employee_loans')->nullable();
            $table->string('all_additions')->nullable();
            $table->string('all_reductions')->nullable();
            $table->string('net_salary')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->enum('status', ['0', '1', '2'])->default('0');
            $table->timestamps();

            $table->foreign('employee_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
