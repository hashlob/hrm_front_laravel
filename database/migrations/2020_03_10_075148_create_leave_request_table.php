<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('leave_type');
            $table->string('reason')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->string('status_message')->nullable();
            $table->integer('next_inline')->default(0);
            $table->unsignedBigInteger('next_inline_id')->nullable();
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('rejected_by')->nullable();
            $table->unsignedBigInteger('applied_by');
            $table->enum('approved_status', ['0', '1', '2', '3'])->default(2);
            $table->timestamps();

            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('cascade');

            $table->foreign('applied_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('rejected_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('next_inline_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('leave_type')
                ->references('id')
                ->on('db_variables_details')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_request');
    }
}
