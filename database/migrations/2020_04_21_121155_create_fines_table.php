<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('amount')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('description')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('user_id');
            $table->enum('status', ['0', '1'])->default('1');
            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')
                ->on('db_variables_details')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fines');
    }
}
