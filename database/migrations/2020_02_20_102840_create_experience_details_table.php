<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperienceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employer')->nullable();
            $table->string('position')->nullable();
            $table->string('joining_date')->nullable();
            $table->string('leaving_date')->nullable();
            $table->string('last_salary')->nullable();
            $table->string('salary_slip')->nullable();
            $table->string('experience_letter')->nullable();
            $table->text('reason_for_leaving')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_details');
    }
}
