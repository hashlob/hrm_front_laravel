<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterviewPanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interview_panels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('interview_lineup_id')->nullable();
            $table->unsignedBigInteger('interviewer_id')->nullable();
            $table->timestamps();

            $table->foreign('interview_lineup_id')
                ->references('id')
                ->on('interview_lineups')
                ->onDelete('cascade');

            $table->foreign('interviewer_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interview_panels');
    }
}
