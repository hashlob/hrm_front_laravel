<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_line', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('request_line_type');
            $table->string('line_name')->nullable(); 
            $table->unsignedBigInteger('manager_id'); 
            $table->unsignedBigInteger('department_id'); 
            $table->integer('order'); 
            $table->enum('status', ['0', '1'])->nullable();
            $table->timestamps();

            $table->foreign('department_id')
            ->references('id')
            ->on('departments')
            ->onDelete('cascade');

            $table->foreign('manager_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->foreign('request_line_type')
            ->references('id')
            ->on('db_variables_details')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_line');
    }
}
