<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveConfigurationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_configuration_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('config_id');
            $table->unsignedBigInteger('leave_type');
            $table->integer('quantity')->nullable();
            $table->enum('status', ['0', '1'])->nullable();
            $table->timestamps();

            $table->foreign('config_id')
            ->references('id')
            ->on('db_variables_details')
            ->onDelete('cascade');

            $table->foreign('leave_type')
            ->references('id')
            ->on('db_variables_details')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_configuration_details');
    }
}
