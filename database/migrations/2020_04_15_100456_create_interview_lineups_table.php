<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterviewLineupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interview_lineups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('interview_date')->nullable();
            $table->string('interview_time')->nullable();
            $table->text('test_details')->nullable();
            $table->text('interview_details')->nullable();
            $table->unsignedBigInteger('cv_id')->nullable();
            $table->enum('status', ['0', '1', '2'])->default('2');
            $table->timestamps();

            $table->foreign('cv_id')
                ->references('id')
                ->on('cv_banks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interview_lineups');
    }
}
