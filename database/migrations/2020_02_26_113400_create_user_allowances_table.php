<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAllowancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_allowances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('value')->nullable();
            $table->string('remarks')->nullable();
            $table->string('applied_on')->nullable();
            $table->boolean('is_monthly')->nullable();
            $table->enum('status', ['0', '1', '2'])->default(1);
            $table->enum('value_type', ['Percent', 'Amount'])->nullable();
            $table->unsignedBigInteger('allowance_type_id');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('allowance_type_id')
                ->references('id')
                ->on('db_variables_details')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_allowances');
    }
}
