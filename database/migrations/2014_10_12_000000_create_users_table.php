<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_number');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('contact_number');
            $table->string('contact_number_two')->nullable();
            $table->string('email');
            $table->string('cnic');
            $table->string('nationality')->nullable();
            $table->enum('gender',['male','female']);
            $table->enum('martial_status',['married','unmarried']);
            $table->string('blood_group')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('address_verification')->nullable();
            $table->string('cnic_attachment')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->text('address')->nullable();
            $table->boolean('is_active')->nullable();
            // $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
