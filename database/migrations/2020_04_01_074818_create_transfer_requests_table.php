<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('from')->nullable();
            $table->unsignedBigInteger('to')->nullable();
            $table->string('request_date')->nullable();
            $table->integer('status')->nullable();
            $table->string('reason')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('initiated_by')->nullable();
            $table->unsignedBigInteger('next_inline')->nullable();
            $table->unsignedBigInteger('rejected_by')->nullable();
            $table->timestamps();

            $table->foreign('from')
                ->references('id')
                ->on('units')
                ->onDelete('cascade');

            $table->foreign('to')
                ->references('id')
                ->on('units')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('next_inline')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('initiated_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('rejected_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_requests');
    }
}
